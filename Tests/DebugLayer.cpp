#include <QtCore/QDebug>

#include "Application/PixmapBuffer.h"

#include "DebugLayer.h"

DebugLayer::DebugLayer(Application *app)
  : Layer(app),
    m_buf(this, PixmapBuffer::RGBA),
    m_buf2(this, PixmapBuffer::RGBA)
{


    qDebug() << "DebugLayer::DebugLayer(): " << this->fieldSize();
  /* resize the buffers in order to use them */
  m_buf.setSize(this->fieldSize());
  m_buf2.setSize(this->fieldSize());
  m_debug_property = this -> createPropertyBuffer();

  this->startTimer(100);

}

void DebugLayer::render()
{

  /* finally render both buffers */
  // m_buf.render();
  // m_buf2.render();
}

void DebugLayer::prepareRender(const QRectF &region)
{


  /* ## here we are going to reduce the physical system to a
   * ## visual representation. Everything that is above 2°C
   * ## is molten water, anything below is ice. */

  /* define scalars that will be used to colorize our pixmap */
  cv::Scalar white(1.0, 1.0, 1.0, 0.5);
  cv::Scalar red(1.0, 0.0, 0.0, 0.75);
  cv::Scalar transp(0.0, 0.0, 0.0, 0.0);

  /* get a shortcut to the temperature property */
  // const cv::Mat H = this->materialProperty(Humidity);

  /* initially set everything to white */
  // m_buf.buffer() = blue;

  /* set the buffer to blue everywhere it is warm enough */
  m_buf.buffer().setTo(transp);
  m_buf2.buffer().setTo(transp);

  m_buf.buffer().setTo(white, m_debug_property > 25);
  m_buf.buffer().setTo(white, m_debug_property < 15);
  // m_buf.buffer().setTo(red, m_debug_property > 40);
  // m_buf.buffer().col(490) = white;
  // m_buf.buffer().setTo(blue3, m_water >= 20);

  /* set the render position */
  m_buf.setViewport(QRectF(-1, -1, 2, 2));
  m_buf2.setViewport(QRectF(-1, -1, 2, 2));

}

void DebugLayer::update(Layer::PropertyTypes t, QRect rect)
{
  /* check if temperature has changed */
  if(t & Temperature) {
    m_debug_property = this->materialProperty(Temperature);
    // cv::blur(m_water, m_water, cv::Size(3, 3));
    emit appearanceChanged(rect);
  }

  // if(t & Humidity)
    // emit appearanceChanged(rect);
}

const cv::Mat DebugLayer::materialProperty(PropertyType t)
{

  /* for anything else we don't care */
  return Layer::materialProperty(t);
}

//add depthFunction 


void DebugLayer::timerEvent(QTimerEvent *event)
{
  // m_buf2Progress += 0.01;

  // qreal angle = m_buf2Progress*2.0*M_PI;

  // m_buf2Pos = QPointF(0.25*sin(angle), 0.25*cos(angle));
  // m_buf2Pos -= QPointF(0.25, 0.25);

  /* signal the change */
  // emit appearanceChanged();

}

