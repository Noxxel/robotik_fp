#ifndef DEBUGLAYER_H
#define DEBUGLAYER_H


#include "Layer/Layer.h"
#include "Application/PixmapBuffer.h"

class DebugLayer : public Layer
{
  Q_OBJECT

  cv::Mat m_debug_property;

  /* create two pixmap buffers,
   * one will have opacity channel, the other
   * don't. */
  PixmapBuffer m_buf;
  PixmapBuffer m_buf2;

public:
  explicit DebugLayer(Application * app);

  const cv::Mat materialProperty(PropertyType t);
  
  void render();
  void prepareRender(const QRectF & region);

  void update(PropertyTypes t, QRect rect);
signals:

public slots:

protected:
  void timerEvent(QTimerEvent * event);

};

#endif // DEBUGLAYER_H
