#include <QtCore/QDebug>

#include "../Application/Application.h"
#include "HandInputLayer.h"
#include "DisplayLayer.h"
#include "RockLayer.h"
#include "CloudLayer.h"
#include "DirtLayer.h"
#include "WaterSimLayer.h"
#include "WaterLayer.h"
#include "TemperatureLayer.h"
#include "VegetationLayer.h"
#include "DebugLayer.h"
#include "PathLayer.h"

#include "Input/TestInput.h"

int main(int argc, char ** argv)
{
	Application app(argc, argv);

    /* set the scaling of depth map. */
    app.setHeightScaling(-0.615, 4100);

	TemperatureLayer * temperaturelayer = new TemperatureLayer(&app);
	// DisplayLayer * disp = new DisplayLayer(&app);
	RockLayer * rocklayer = new RockLayer(&app);
	WaterSimLayer * watersimlayer = new WaterSimLayer (&app);
	DirtLayer * dirtlayer = new DirtLayer(&app);
	WaterLayer * waterlayer = new WaterLayer(&app);
	VegetationLayer * vegetationlayer = new VegetationLayer(&app);
	CloudLayer * cloudlayer = new CloudLayer(&app);
	PathLayer * pathlayer = new PathLayer(&app);
	HandInputLayer * handinputlayer = new HandInputLayer(&app);
	DebugLayer * debuglayer = new DebugLayer(&app);

	/* add layers to application */

	app.addLayer(temperaturelayer);
	// app.addLayer(disp);
	app.addLayer(rocklayer);
	app.addLayer(watersimlayer);
	app.addLayer(dirtlayer);
	app.addLayer(waterlayer);
	app.addLayer(vegetationlayer);
	app.addLayer(cloudlayer);
	app.addLayer(pathlayer);
	app.addLayer(handinputlayer);
	app.addLayer(debuglayer);

	QObject::connect(handinputlayer, &HandInputLayer::transmitCoords, pathlayer, &PathLayer::receiveCoords);
	QObject::connect(handinputlayer, &HandInputLayer::triggerRain, watersimlayer, &WaterSimLayer::rainTriggered);
	QObject::connect(handinputlayer, &HandInputLayer::removeWater, watersimlayer, &WaterSimLayer::waterRemoved);
	QObject::connect(handinputlayer, &HandInputLayer::resetPath, pathlayer, &PathLayer::pathReset);

	/* if you want to use dynamic components like QTimer to perform updates,
	* you must run the event loop */
	app.start();

	return 0;
}
