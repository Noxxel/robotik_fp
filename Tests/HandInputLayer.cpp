#include <QtCore/QDebug>

#include "Application/PixmapBuffer.h"

#include "HandInputLayer.h"


HandInputLayer::HandInputLayer(Application *app)
  : Layer(app),
    m_buf(this, PixmapBuffer::RGBA)
{


  qDebug() << "HandInputLayer::HandInputLayer(): " << this->fieldSize();

  /* resize the buffers in order to use them */
  m_buf.setSize(this->fieldSize());

  m_depth = this -> createPropertyBuffer();
  m_render = this -> createPropertyBuffer();

  command_accepted = false;

  this->startTimer(200);

}

void HandInputLayer::render()
{

  /* finally render both buffers */
  m_buf.render();
}

void HandInputLayer::prepareRender(const QRectF &region)
{


  /* ## here we are going to reduce the physical system to a
   * ## visual representation. Everything that is above 2°C
   * ## is molten water, anything below is ice. */

  /* define scalars that will be used to colorize our pixmap */
  const cv::Scalar transp(0.0, 0.0, 0.0, 0);
  const cv::Scalar white(1.0, 1.0, 1.0, 1.0);
  const cv::Scalar yellow(1.0, 1.0, 0.0, 1.0);
  const cv::Scalar red(1.0, 0.0, 0.0, 1.0);
  // cv::Scalar tempBlack(0.0, 0.0, 0.0, 1);

  // /* set the buffer to lightblue below 20 */
  m_buf.buffer() = transp;
  if(command_accepted){
    m_buf.buffer().setTo(yellow, m_render > 0.1);
  }else{
    m_buf.buffer().setTo(white, m_render > 0.1);
  }
  // m_buf.buffer().setTo(red, m_test == 255);
    // m_buf.buffer().setTo(red, m_render > 100);
  // m_buf.buffer().setTo(tempBlack, m_temperature<=35);
  // m_buf.buffer().setTo(lightblue, m_buf.buffer() == tempBlack);


  // /* set the render position */
  m_buf.setViewport(QRectF(-1, -1, 2, 2));

}

void HandInputLayer::update(Layer::PropertyTypes t, QRect rect)
{

}


const cv::Mat HandInputLayer::materialProperty(PropertyType t)
{
  /* for anything else we don't care */
  return Layer::materialProperty(t);
}


void HandInputLayer::timerEvent(QTimerEvent *event)
{

  if(!command_accepted){
    if(t_remove < 5){
      if(t_recognize < 10){
        handStill();
      }else{
        commandRecognized();
        t_recognize = 0;
      }
      t_remove++;
    }else if(t_remove == 5){
      m_render.setTo(0);
      emit appearanceChanged();
      t_remove = 5;
    }
  }else if(t_command < 8){
    t_command++;
  }else{
    t_command = 0;
    command_accepted = false;
  }

}

void HandInputLayer::handStill(){
  if(new_data){
    new_data = false;
    if(v_points_between_fingers_last.size() >= 1 && v_points_between_fingers.size() == v_points_between_fingers_last.size()){
      bool not_same = false;
      for(int i = 0; i < v_points_between_fingers.size(); i++){
        if(std::abs(v_points_between_fingers.at(i).y - v_points_between_fingers_last.at(i).y) < 12){
          if(std::abs(v_points_between_fingers.at(i).x - v_points_between_fingers_last.at(i).x) < 12){
            // std::cout<<"same counter: "<<t_recognize<<std::endl;
          }else{
            not_same = true;
          }
        }else{
          not_same = true;
        }
      }
      if(not_same){
        t_recognize = 0;
      }else{
        t_recognize++;
      }
    }else{
      t_recognize = 0;
    }
  }
}

void HandInputLayer::commandRecognized(){
  if(v_points_between_fingers_last.size() == 1){
    emit transmitCoords(v_points_between_fingers_last.at(0));
    t_recognize = 0;
  }else if(v_points_between_fingers_last.size() >= 4){
    emit triggerRain();
    t_recognize = 0;
  }else if(v_points_between_fingers_last.size() == 3){
    emit removeWater();
    t_recognize = 0;
  }else if(v_points_between_fingers_last.size() == 2){
    emit resetPath();
    t_recognize = 0;
  }

  command_accepted = true;
  emit appearanceChanged();
}

void HandInputLayer::updateDepthHigh(QRect region)
{

  /* set the height */
  if(!command_accepted){
    new_data = true;

    depth().copyTo(m_depth);
    m_depth.convertTo(m_depth, CV_8UC1, 1);
    m_depth.setTo(0, m_depth >= 500);
    m_depth.setTo(255, m_depth > 130);
    m_depth.setTo(255, m_depth < 0);
    m_depth.setTo(0, m_depth != 255);

    // m_depth.copyTo(m_test);

    cv::Mat m_threshold;
    cv::blur(m_depth, m_depth, cv::Size(3, 3));
    std::vector<std::vector<cv::Point>> v_contours;

    cv::findContours(m_depth, v_contours, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE);

    //find biggest contour
    int high_spot = -1;
    int current_high = 0;
    for(int i = 0; i < v_contours.size(); i++){
      if(v_contours.at(i).size() > current_high){
        current_high = v_contours.at(i).size();
        high_spot = i;
      }
    }

    //find defects

    if(high_spot != -1){
      if(v_contours.at(high_spot).size() > 5){
        std::vector<int> v_hull;
        cv::convexHull(v_contours.at(high_spot), v_hull, false, false);
        if(v_hull.size() > 3){
          std::vector<cv::Vec4i> v_c_defects;
          cv::convexityDefects(v_contours.at(high_spot), v_hull, v_c_defects);
          m_render.setTo(0);

          // std::cout<<v_c_defects.size()<<std::endl;

          //use all defects that are big enough
          int temp_y;
          int temp_x;
          float temp_y_mod;
          float temp_x_mod;
          float distance;
          v_points_between_fingers_last.clear();
          v_points_between_fingers_last = v_points_between_fingers;
          v_points_between_fingers.clear();
          for(int i = 0; i < v_c_defects.size(); i++){
            // std::cout<<v_c_defects.at(i)[3]<<std::endl;
            if(v_c_defects.at(i)[3]/256 > 30){
              temp_y = (v_contours.at(high_spot).at(v_c_defects.at(i)[0]).y + v_contours.at(high_spot).at(v_c_defects.at(i)[1]).y)/2;
              temp_x = (v_contours.at(high_spot).at(v_c_defects.at(i)[0]).x + v_contours.at(high_spot).at(v_c_defects.at(i)[1]).x)/2;
              temp_y_mod = std::abs(v_contours.at(high_spot).at(v_c_defects.at(i)[0]).y - v_contours.at(high_spot).at(v_c_defects.at(i)[1]).y);
              temp_x_mod = std::abs(v_contours.at(high_spot).at(v_c_defects.at(i)[0]).x - v_contours.at(high_spot).at(v_c_defects.at(i)[1]).x);

              if(temp_y < temp_x){
                temp_y_mod *= 0.41;
              }else{
                temp_x_mod *= 0.41;
              }

              distance = temp_x_mod + temp_y_mod;
                // std::cout<<distance<<"  "<<std::endl;
              if(distance < 85){
                m_render.at<float>(temp_y, temp_x) = 30;
                v_points_between_fingers.push_back(cv::Point(temp_x, temp_y));
              }
            }
          }

          for(int i = 0; i < 3; i++){
            cv::GaussianBlur(m_render, m_render, cv::Size(5,5), 5.0);
          }

          // for(int i = 0; i < v_contours.at(high_spot).size(); i++){
          //   m_render.at<float>(v_contours.at(high_spot).at(i).y, v_contours.at(high_spot).at(i).x) = 1;
          // }

          // cv::blur(m_render, m_render, cv::Size(2,2));

          emit appearanceChanged();
        }
      }
    }
  }



  t_remove = 0;
}

