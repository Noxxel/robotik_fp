#ifndef HANDINPUTLAYER_H
#define HANDINPUTLAYER_H


#include "Layer/Layer.h"
#include "Application/PixmapBuffer.h"

class HandInputLayer : public Layer
{
  Q_OBJECT

  /* create two pixmap buffers,
   * one will have opacity channel, the other
   * don't. */
  PixmapBuffer m_buf;
  cv::Mat m_depth;
  cv::Mat m_render;
  std::vector<cv::Point> v_points_between_fingers;
  std::vector<cv::Point> v_points_between_fingers_last;

  bool command_accepted;
  bool new_data{false};
  int t_remove{0};
  int t_recognize{0};
  int t_command{0};

public:
  explicit HandInputLayer(Application * app);
  
  const cv::Mat materialProperty(PropertyType t);

  void render();
  void prepareRender(const QRectF & region);

  void update(PropertyTypes t, QRect rect);
  void updateDepthHigh(QRect region);
signals:
  void transmitCoords(cv::Point received_point);
  void triggerRain();
  void removeWater();
  void resetPath();

public slots:

protected:
  void timerEvent(QTimerEvent * event);

  void handStill();
  void commandRecognized();

};

#endif // HANDINPUTLAYER_H
