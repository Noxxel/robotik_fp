#include <QtCore/QDebug>

#include "Application/PixmapBuffer.h"

#include "WaterLayer.h"

WaterLayer::WaterLayer(Application *app)
  : Layer(app),
    m_buf(this, PixmapBuffer::RGBA)
{


    qDebug() << "WaterLayer::WaterLayer(): " << this->fieldSize();
  /* resize the buffers in order to use them */
  m_buf.setSize(this->fieldSize());
  m_water = this -> createPropertyBuffer();
  m_water_render = cv::Mat(m_water.size().height, m_water.size().width,
                   CV_32FC4, 0.0);

  m_water_channel1 = this -> createPropertyBuffer();
  m_water_channel2 = this -> createPropertyBuffer();
  m_water_channel3 = this -> createPropertyBuffer();
  m_water_channel4 = this -> createPropertyBuffer();
  // m_water_squish = this -> createPropertyBufferSquish();

  // m_water_temp = this -> createPropertyBuffer();

  this->startTimer(100);

}

void WaterLayer::render()
{

  /* finally render both buffers */
  m_buf.render();
}

void WaterLayer::prepareRender(const QRectF &region)
{


  /* ## here we are going to reduce the physical system to a
   * ## visual representation. Everything that is above 2°C
   * ## is molten water, anything below is ice. */

  /* define scalars that will be used to colorize our pixmap */
  const cv::Scalar blue1(0.0, 1.0, 0.0, 0.8);
  const cv::Scalar blue2(0.0, 0.0, 0.8, 0.9);
  const cv::Scalar blue3(0.0, 0.0, 0.6, 0.95);
  const cv::Scalar blue4(0.0, 0.0, 0.4, 1.0);
  const cv::Scalar sand(0.95, 0.8, 0, 1);
  const cv::Scalar transp(0.0, 0.0, 0.0, 0.0);

  /* get a shortcut to the temperature property */
  // const cv::Mat H = this->materialProperty(Humidity);

  /* initially set everything to white */
  m_buf.buffer() = transp;

  /* set the buffer to blue everywhere it is warm enough */
  m_water_render.copyTo(m_buf.buffer());
  // m_buf.buffer().setTo(sand, H >= 0.75);
  // m_buf.buffer().setTo(blue1, W >= 0.1);
  // std::cout<<cv::Mat(m_water, cv::Rect(620, 350, 10, 10))<<std::endl;
  // m_buf.buffer().setTo(blue2, m_water >= 1);
  // m_buf.buffer().setTo(blue3, m_water >= 15);
  // m_buf.buffer().setTo(blue4, m_water >= 30);

  /* set the render position */
  m_buf.setViewport(QRectF(-1, -1, 2, 2));

}

void WaterLayer::update(Layer::PropertyTypes t, QRect rect)
{
  /* check if temperature has changed */
  if(t & Water) {
    Layer::materialProperty(Water).copyTo(m_water);
    // m_water.copyTo(m_water_temp);
    cv::blur(m_water, m_water, cv::Size(5, 5));
    // cv::resize(m_water, m_water_squish, m_water_squish.size(), 0, 0);

    channel_mats.clear();

    //red
    m_water_channel1.setTo(0);
    channel_mats.push_back(m_water_channel1);

    //green
    m_water_channel2.setTo(0);
    channel_mats.push_back(m_water_channel2);

    //blue
    m_water.copyTo(m_water_channel3);
    m_water_channel3 /= -50;
    m_water_channel3 += 0.8;
    m_water_channel3.setTo(0.7, m_water_channel3 > 0.7);
    m_water_channel3.setTo(0.25, m_water_channel3 < 0.25);
    channel_mats.push_back(m_water_channel3);

    //alpha
    m_water_channel4.setTo(0);
    m_water_channel4.setTo(1, m_water >= 1);
    channel_mats.push_back(m_water_channel4);

    // m_water_render.setTo(0);
    cv::merge(channel_mats, m_water_render);

    emit appearanceChanged();
  }

  // if(t & Humidity)
    // emit appearanceChanged(rect);
}

const cv::Mat WaterLayer::materialProperty(PropertyType t)
{

  /* for anything else we don't care */
  return Layer::materialProperty(t);
}

//add depthFunction 


void WaterLayer::timerEvent(QTimerEvent *event)
{
  // m_buf2Progress += 0.01;

  // qreal angle = m_buf2Progress*2.0*M_PI;

  // m_buf2Pos = QPointF(0.25*sin(angle), 0.25*cos(angle));
  // m_buf2Pos -= QPointF(0.25, 0.25);

  /* signal the change */
  // emit appearanceChanged();

}

