#include <QtCore/QDebug>
#include <QtCore/QTimer>
#include <QtCore/QTime>

#include "Application/PixmapBuffer.h"

#include "WaterSimLayer.h"

WaterSimLayer::WaterSimLayer(Application *app)
  : Layer(app)//,
    // m_buf(this, PixmapBuffer::RGBA)
{


  qDebug() << "WaterSimLayer::WaterSimLayer(): " << this->fieldSize();
  /* resize the buffers in order to use them */
  // m_buf.setSize(this->fieldSize());

  this->startTimer(150);

  m_depth = this -> createPropertyBuffer();
  m_depth_mod = this -> createPropertyBuffer();
  m_current_spread = this -> createPropertyBuffer();
  m_fert = this -> createPropertyBuffer();
  m_humid = this -> createPropertyBuffer();
  m_temperature = this -> createPropertyBuffer();
  m_humid_temp = this -> createPropertyBuffer();
  m_evaporate_humidity = this -> createPropertyBuffer();

  m_rain_one = this -> createPropertyBufferSquish();
  m_rain_two = this -> createPropertyBufferSquish();
  m_rain_three = this -> createPropertyBufferSquish();
  m_rain_four = this -> createPropertyBufferSquish();
  m_rain_five = this -> createPropertyBufferSquish();

  m_clouds = this -> createPropertyBuffer();
  m_clouds_squished = this -> createPropertyBufferSquish();

  m_depth_squish = this -> createPropertyBufferSquish();
  m_water = this -> createPropertyBuffer();
  m_water_squish = this -> createPropertyBufferSquish();
  m_water_static = this -> createPropertyBufferSquish();
  m_evaporate = this -> createPropertyBufferSquish();
  m_temp_rain = this -> createPropertyBufferSquish();
  m_depth_last = this -> createPropertyBufferSquish();
  m_humid.setTo(0);
  m_depth.setTo(0);
  m_depth_squish.setTo(0);
  m_evaporate.setTo(0.025);
  m_evaporate_humidity.setTo(0.0015);
  m_water.setTo(0);
  m_water_squish.setTo(0);
  m_depth_mod.setTo(0);
  area_counter = 0;

  start_rain = false;
  rain_time = false;
  rain_timer = 0;
  cloud_timer = 0;

}

void WaterSimLayer::render()
{

  /* finally render both buffers */
  // m_buf.render();
}

void WaterSimLayer::prepareRender(const QRectF &region)
{

}

void WaterSimLayer::update(Layer::PropertyTypes t, QRect rect)
{
  /* check if Water has changed */
  if(t & Water) {
    // QTime timer;
    // timer.start();

    m_water_squish -= m_evaporate;
    m_water_squish.setTo(0, m_water_squish < 0);

    // printWater();
    // cv::resize(m_water, m_water_squish, m_water_squish.size(), 0, 0);
    m_water_squish.copyTo(m_water_static);
    findWater();
    if(start_rain){
      addRain();
      addClouds();
    }
    if(rain_time){
      if(cloud_timer > 35){
        addWater();
        removeClouds();
      }else{
        cloud_timer++;
      }
      // std::cout<<cv::Mat(m_clouds, cv::Rect(290, 220, 40, 40))<<std::endl;
    }
    cv::resize(m_water_squish, m_water, m_water.size(), 0, 0);
    // std::cout<<cv::Mat(m_water_squish, cv::Rect(620/4, 350/4, 10/5, 10/5))<<std::endl;
    // std::cout<<cv::Mat(m_water, cv::Rect(620, 350, 10, 10))<<std::endl;
    
    m_humid -= m_evaporate_humidity;
    m_humid.setTo(0, m_humid < 0);

    // m_water.copyTo(m_humid, m_water < 0.25); //caused fucked up shit with humidity
		m_humid.setTo(1, m_water >= 1);

    cv::GaussianBlur(m_humid, m_humid_temp, cv::Size(15,15), 5.0);
    m_humid_temp.setTo(0, m_water >= 1);

    double min,max;
    minMaxLoc(m_humid_temp, &min, &max);
    s_sum = cv::sum(m_water_squish);
    sum = s_sum[0];

    // std::cout<<sum<<std::endl;

    if(max > 0.25 && sum > 500){
      m_humid_temp /= max;
    }

    m_humid_temp.copyTo(m_humid, m_humid_temp > m_humid);

    // qDebug() << "\n (" << timer.elapsed() << " ms)";
  }

  if(t & Temperature){
    Layer::materialProperty(Temperature).copyTo(m_temperature);
    m_humid.copyTo(m_humid_temp);
    m_humid_temp.setTo(1, m_water >= 1);
    m_humid_temp *= -0.45;
    m_humid_temp += 1.45;
    cv::multiply(m_temperature, m_humid_temp, m_temperature);
  }

  if(t & Fertility){
    Layer::materialProperty(Fertility).copyTo(m_fert);
    m_fert.setTo(0, m_water >= 1);
  }
}

const cv::Mat WaterSimLayer::materialProperty(PropertyType t)
{
  //handle humidity
  if(t == Layer::Humidity)
    return m_humid;

  if(t == Layer::Fertility)
    return m_fert;

  if(t == Layer::Water)
  	return m_water;

  if(t == Layer::Temperature)
    return m_temperature;

  if(t == Layer::Clouds)
    return m_clouds;

  /* for anything else we don't care */
  return Layer::materialProperty(t);
}


void WaterSimLayer::timerEvent(QTimerEvent *event)
{
	if(!(t_progress < 15)){
    emit materialPropertyChanged(Water | Humidity | Fertility | Temperature);
	}else{
    t_progress++;
  }
  if(!start_rain && !rain_time){
    rain_timer++;
    if(rain_timer > 500){
      rain_timer = 0;
      start_rain = true;
    }
  }
}


//is called through handledepthinput from application.cpp
void WaterSimLayer::updateDepth(QRect region)
{
  /* set the height */
  depth().copyTo(m_depth);
  cv::resize(m_depth, m_depth_squish, m_depth_squish.size(), 0, 0);
  m_depth_squish *= 10;
  emit materialPropertyChanged(Water);
}

void WaterSimLayer::findWater(){

  for(int j = y_low_edge/5; j < m_water_squish.rows - y_high_edge/5; j++){
    for(int k = x_low_edge/5; k < m_water_squish.cols - x_high_edge/5; k++){
      if(m_water_static.at<float>(j, k) > 0.25){

        v_spread_data.clear();
        temp.clear();
        temp.push_back(m_water_squish.at<float>(j-1, k) + m_depth_squish.at<float>(j-1, k) / non_diagonal_support); // gotta help these poor values out, otherwise they get opressed by the mean diagonals!
        temp.push_back(m_water_squish.at<float>(j, k+1) + m_depth_squish.at<float>(j, k+1) / non_diagonal_support); // gotta help these poor values out, otherwise they get opressed by the mean diagonals!
        temp.push_back(m_water_squish.at<float>(j+1, k) + m_depth_squish.at<float>(j+1, k) / non_diagonal_support); // gotta help these poor values out, otherwise they get opressed by the mean diagonals!
        temp.push_back(m_water_squish.at<float>(j, k-1) + m_depth_squish.at<float>(j, k-1) / non_diagonal_support); // gotta help these poor values out, otherwise they get opressed by the mean diagonals!
        temp.push_back(m_water_squish.at<float>(j-1, k-1) + m_depth_squish.at<float>(j-1, k-1));
        temp.push_back(m_water_squish.at<float>(j-1, k+1) + m_depth_squish.at<float>(j-1, k+1));
        temp.push_back(m_water_squish.at<float>(j+1, k+1) + m_depth_squish.at<float>(j+1, k+1));
        temp.push_back(m_water_squish.at<float>(j+1, k-1) + m_depth_squish.at<float>(j+1, k-1));
        v_spread_data.push_back(temp);
        temp.clear();
        temp.push_back(0);
        temp.push_back(1);
        temp.push_back(2);
        temp.push_back(3);
        temp.push_back(4);
        temp.push_back(5);
        temp.push_back(6);
        temp.push_back(7);
        v_spread_data.push_back(temp);

        //sort it
        v_val.clear();
        v_loc.clear();
        while(v_spread_data.at(0).size() > 0){
          float lowest_val = 1000000;
          float lowest_loc = -1;
          int where = -1;
          for(unsigned int l = 0; l < v_spread_data.at(0).size(); l++){
            if(v_spread_data.at(0).at(l) < lowest_val){
              lowest_val = v_spread_data.at(0).at(l);
              lowest_loc = v_spread_data.at(1).at(l);
              where = l;
            }
          }
          v_val.push_back(lowest_val);
          v_loc.push_back(lowest_loc);
          v_spread_data.at(0).erase(v_spread_data.at(0).begin() + where);
          v_spread_data.at(1).erase(v_spread_data.at(1).begin() + where);
        }
        v_spread_data.clear();

        v_spread_data.push_back(v_val);
        v_spread_data.push_back(v_loc);
        temp.clear();
        temp.push_back(m_water_static.at<float>(j, k));
        temp.push_back(m_water_static.at<float>(j, k) + m_depth_squish.at<float>(j, k));
        v_spread_data.push_back(temp);

        float current_water = m_water_static.at<float>(j, k);

        // for(int q = 0; q < v_spread_data.at(0).size(); q++){
        //   std::cout<<v_spread_data.at(0).at(q)<<"  ";
        // }
        // std::cout<<std::endl;

        spreadWater(1);


        // std::cout<<"_________________________________________________________"<<std::endl;
        // std::cout<<cv::Mat(m_depth_squish, cv::Rect(k-1, j-1, 3, 3))<<std::endl;
        // std::cout<<cv::Mat(m_water_squish, cv::Rect(k-1, j-1, 3, 3))<<std::endl;

        // apply water changes to water matrix;
        for(unsigned int n = 0; n < v_spread_data.at(0).size(); n++){
          if(v_spread_data.at(1).at(n) == 0){
              m_water_squish.at<float>(j-1, k) += ((v_spread_data.at(0).at(n) - m_depth_squish.at<float>(j-1, k) / non_diagonal_support) - m_water_squish.at<float>(j-1, k));
          }
          if(v_spread_data.at(1).at(n) == 1){
              m_water_squish.at<float>(j, k+1) += ((v_spread_data.at(0).at(n) - m_depth_squish.at<float>(j, k+1) / non_diagonal_support) - m_water_squish.at<float>(j, k+1));
          }
          if(v_spread_data.at(1).at(n) == 2){
              m_water_squish.at<float>(j+1, k) += ((v_spread_data.at(0).at(n) - m_depth_squish.at<float>(j+1, k) / non_diagonal_support) - m_water_squish.at<float>(j+1, k));
          }
          if(v_spread_data.at(1).at(n) == 3){
              m_water_squish.at<float>(j, k-1) += ((v_spread_data.at(0).at(n) - m_depth_squish.at<float>(j, k-1) / non_diagonal_support) - m_water_squish.at<float>(j, k-1));
          }
          if(v_spread_data.at(1).at(n) == 4){
              m_water_squish.at<float>(j-1, k-1) += ((v_spread_data.at(0).at(n) - m_depth_squish.at<float>(j-1, k-1)) - m_water_squish.at<float>(j-1, k-1));
          }
          if(v_spread_data.at(1).at(n) == 5){
              m_water_squish.at<float>(j-1, k+1) += ((v_spread_data.at(0).at(n) - m_depth_squish.at<float>(j-1, k+1)) - m_water_squish.at<float>(j-1, k+1));
          }
          if(v_spread_data.at(1).at(n) == 6){
              m_water_squish.at<float>(j+1, k+1) += ((v_spread_data.at(0).at(n) - m_depth_squish.at<float>(j+1, k+1)) - m_water_squish.at<float>(j+1, k+1));
          }
          if(v_spread_data.at(1).at(n) == 7){
              m_water_squish.at<float>(j+1, k-1) += ((v_spread_data.at(0).at(n) - m_depth_squish.at<float>(j+1, k-1)) - m_water_squish.at<float>(j+1, k-1));
          }
      	}

        m_water_squish.at<float>(j, k) -= (current_water - v_spread_data.at(2).at(0));


        // std::cout<<cv::Mat(m_water_squish, cv::Rect(k-1, j-1, 3, 3))<<std::endl;

        // throw cv::Exception();
      }
    }
  }
}

void WaterSimLayer::spreadWater(int looking_at){
  // for(int q = 0; q < v_spread_data.at(0).size(); q++){
  //   std::cout<<v_spread_data.at(0).at(q)<<"  ";
  // }
  // std::cout<<std::endl;
  // std::cout<<"my water: "<<v_spread_data.at(2).at(0)<<std::endl;

  float looking_at_f = looking_at;
  float level_difference_me = v_spread_data.at(2).at(1) - v_spread_data.at(0).at(0);
  float our_water = v_spread_data.at(2).at(0);
  if(level_difference_me > 0){
    float level_difference_them = v_spread_data.at(0).at(looking_at_f) - v_spread_data.at(0).at(0);
    if(level_difference_them > level_difference_me/(looking_at_f+1)){//they can't get enough to reach the next (based on level)
      if(level_difference_me*(looking_at_f/(looking_at_f+1)) > our_water){//they want more than i have
        for(int i = 0; i < looking_at_f; i++){
          v_spread_data.at(0).at(i) += our_water/looking_at_f;
        }
        v_spread_data.at(2).at(0) -= our_water;
        v_spread_data.at(2).at(1) -= our_water; //nothing left!
      }else{//give them everything they want, we all split evenly
        for(int i = 0; i < looking_at_f; i++){
          v_spread_data.at(0).at(i) += level_difference_me/(looking_at_f+1);
        }
        v_spread_data.at(2).at(0) -= level_difference_me*(looking_at_f/(looking_at_f+1));
        v_spread_data.at(2).at(1) -= level_difference_me*(looking_at_f/(looking_at_f+1));//all of them, including me are on the same level now!
      }
    }else{//based on level they can get enough to reach the next
      if(level_difference_them*looking_at_f > our_water){//they can't get enough to reach the next (based on water)
        if(level_difference_me*(looking_at_f/(looking_at_f+1)) > our_water){//they want more than i have
          for(int i = 0; i < looking_at_f; i++){
            v_spread_data.at(0).at(i) += our_water/looking_at_f;
          }
          v_spread_data.at(2).at(0) -= our_water;
          v_spread_data.at(2).at(1) -= our_water; //nothing left!
        }else{//give them everything they want, we all split evenly
          for(int i = 0; i < looking_at_f; i++){
            v_spread_data.at(0).at(i) += level_difference_me/(looking_at_f+1);
          }
          v_spread_data.at(2).at(0) -= level_difference_me*(looking_at_f/(looking_at_f+1));
          v_spread_data.at(2).at(1) -= level_difference_me*(looking_at_f/(looking_at_f+1));//all of them, including me are on the same level now!
        }
      }else{//they can get enough to reach the next (based on level as well as water)
        for(int i = 0; i < looking_at_f; i ++){
          v_spread_data.at(0).at(i) += level_difference_them;
        }
        v_spread_data.at(2).at(0) -= level_difference_them*looking_at_f;
        v_spread_data.at(2).at(1) -= level_difference_them*looking_at_f;
        if(looking_at_f+1 < 8){
          spreadWater(looking_at+1);
        }else{
          spreadWaterLast(looking_at+1);
        }
      }
    }
  }
}

void WaterSimLayer::spreadWaterLast(int looking_at){
  float looking_at_f = looking_at;
  float level_difference_me = v_spread_data.at(2).at(1) - v_spread_data.at(0).at(0);
  float our_water = v_spread_data.at(2).at(0);
  if(level_difference_me > 0){
    if(level_difference_me*(looking_at_f/(looking_at_f+1)) > our_water){//i give him everything i have
      for(int i = 0; i < looking_at_f; i++){
        v_spread_data.at(0).at(i) += our_water/looking_at_f;
      }
      v_spread_data.at(2).at(0) -= our_water;
      v_spread_data.at(2).at(1) -= our_water;
    }else{//i give him everything he wants + i have spare! bot on same level!
      for(int i = 0; i < looking_at_f; i++){
        v_spread_data.at(0).at(i) += level_difference_me/(looking_at_f+1);
      }
      v_spread_data.at(2).at(0) -= level_difference_me*(looking_at_f/(looking_at_f+1));
      v_spread_data.at(2).at(1) -= level_difference_me*(looking_at_f/(looking_at_f+1));//all of them, including me are on the same level now!
    }
  }
}

void WaterSimLayer::addRain(){
  s_sum = cv::sum(m_water_squish);
  sum = s_sum[0];

  rain_data.clear();
  cv::Mat m_depth_current;
  m_depth_current = this -> createPropertyBuffer();
  m_depth.copyTo(m_depth_current);
  m_depth_current.setTo(-100, m_depth_current > 450);

  int rain_counter = 0;

  double min, max;
  cv::Point minLoc, maxLoc;
  bool viable_found;
  int viable_level;
  int min_viable_area, max_viable_area;
  min_viable_area = 800;
  max_viable_area = 2200;

  for(int j = 0; j < 5; j++){
    cv::minMaxLoc(m_depth_current, &min, &max, &minLoc, &maxLoc);
    viable_found = false;
    viable_level = 0;
    for(int i = 10; i >= 2; i--){
      if(!viable_found){
        m_depth_current.copyTo(m_depth_mod);
        area_counter = 0;
        m_current_spread.setTo(0);
        spread(maxLoc.y, maxLoc.x, i, max);
      }
      if(area_counter > min_viable_area && area_counter < max_viable_area && max >= 80){
        viable_found = true;
        viable_level = i;
      }
    }
    m_depth_current.copyTo(m_depth_last);
    m_depth_current.setTo(-100, m_current_spread == 1);

    float missing_water = max_water - sum;
    if(viable_found){
      rain_counter++;
      if(rain_counter == 1){
        cv::resize(m_current_spread, m_rain_one, m_rain_one.size(), 0, 0);
        m_depth_last.copyTo(m_depth_mod);
        area_counter = 0;
        m_current_spread.setTo(0);
        spread(maxLoc.y, maxLoc.x, viable_level-0.2, max);
        cv::resize(m_current_spread, m_temp_rain, m_temp_rain.size(), 0, 0);
        m_rain_one -= m_temp_rain;

        decimateRain(rain_counter);
        fixRain(rain_counter);
    // std::cout<<cv::Mat(m_rain_one, cv::Rect(80, 40, 20, 20))<<std::endl;

        m_rain_one.setTo(1, m_rain_one > 0);
        m_rain_one.setTo(0, m_rain_one < 1);
        area_counter = cv::countNonZero(m_rain_one);
        if(area_counter > 0){
          m_rain_one *= 2;
          rain_data.push_back((missing_water/(5*2))/area_counter);
        }else{
          rain_data.push_back(0);
        }
      }
      if(rain_counter == 2){
        cv::resize(m_current_spread, m_rain_two, m_rain_two.size(), 0, 0);
        m_depth_last.copyTo(m_depth_mod);
        area_counter = 0;
        m_current_spread.setTo(0);
        spread(maxLoc.y, maxLoc.x, viable_level-0.2, max);
        cv::resize(m_current_spread, m_temp_rain, m_temp_rain.size(), 0, 0);
        m_rain_two -= m_temp_rain;

        decimateRain(rain_counter);
        fixRain(rain_counter);

        m_rain_two.setTo(1, m_rain_two > 0);
        m_rain_two.setTo(0, m_rain_two < 1);
        area_counter = cv::countNonZero(m_rain_two);
        if(area_counter > 0){
          m_rain_two *= 2;
          rain_data.push_back((missing_water/(5*2))/area_counter);
        }else{
          rain_data.push_back(0);
        }
      }
      if(rain_counter == 3){
        cv::resize(m_current_spread, m_rain_three, m_rain_three.size(), 0, 0);
        m_depth_last.copyTo(m_depth_mod);
        area_counter = 0;
        m_current_spread.setTo(0);
        spread(maxLoc.y, maxLoc.x, viable_level-0.2, max);
        cv::resize(m_current_spread, m_temp_rain, m_temp_rain.size(), 0, 0);
        m_rain_three -= m_temp_rain;

        decimateRain(rain_counter);
        fixRain(rain_counter);

        m_rain_three.setTo(1, m_rain_three > 0);
        m_rain_three.setTo(0, m_rain_three < 1);
        area_counter = cv::countNonZero(m_rain_three);
        if(area_counter > 0){
          m_rain_three *= 2;
          rain_data.push_back((missing_water/(5*2))/area_counter);
        }else{
          rain_data.push_back(0);
        }
      }
      if(rain_counter == 4){
        cv::resize(m_current_spread, m_rain_four, m_rain_four.size(), 0, 0);
        m_depth_last.copyTo(m_depth_mod);
        area_counter = 0;
        m_current_spread.setTo(0);
        spread(maxLoc.y, maxLoc.x, viable_level-0.2, max);
        cv::resize(m_current_spread, m_temp_rain, m_temp_rain.size(), 0, 0);
        m_rain_four -= m_temp_rain;

        decimateRain(rain_counter);
        fixRain(rain_counter);

        m_rain_four.setTo(1, m_rain_four > 0);
        m_rain_four.setTo(0, m_rain_four < 1);
        area_counter = cv::countNonZero(m_rain_four);
        if(area_counter > 0){
          m_rain_four *= 2;
          rain_data.push_back((missing_water/(5*2))/area_counter);
        }else{
          rain_data.push_back(0);
        }
      }
      if(rain_counter == 5){
        cv::resize(m_current_spread, m_rain_five, m_rain_five.size(), 0, 0);
        m_depth_last.copyTo(m_depth_mod);
        area_counter = 0;
        m_current_spread.setTo(0);
        spread(maxLoc.y, maxLoc.x, viable_level-0.2, max);
        cv::resize(m_current_spread, m_temp_rain, m_temp_rain.size(), 0, 0);
        m_rain_five -= m_temp_rain;

        decimateRain(rain_counter);
        fixRain(rain_counter);

        m_rain_five.setTo(1, m_rain_five > 0);
        m_rain_five.setTo(0, m_rain_five < 1);
        area_counter = cv::countNonZero(m_rain_five);
        if(area_counter > 0){
          m_rain_five *= 2;
          rain_data.push_back((missing_water/(5*2))/area_counter);
        }else{
          rain_data.push_back(0);
        }
      }
    }
  }
  rain_data.emplace(rain_data.begin(), rain_counter);
  start_rain = false;
  rain_time =true;

  std::cout<<rain_data.at(0)<<" rain areas created."<<std::endl;
}

void WaterSimLayer::addWater(){
  bool done = true;
  for(int i = 1; i <= rain_data.at(0); i++){
    if(i == 1 && rain_data.at(1) > 0){
      rain_data.at(1) -= 1;
      m_water_squish += m_rain_one;
      done = false;
    }else if(i == 2 && rain_data.at(2) > 0){
      rain_data.at(2) -= 1;
      m_water_squish += m_rain_two;
      done = false;
    }else if(i == 3 && rain_data.at(3) > 0){
      rain_data.at(3) -= 1;
      m_water_squish += m_rain_three;
      done = false;
    }else if(i == 4 && rain_data.at(4) > 0){
      rain_data.at(4) -= 1;
      m_water_squish += m_rain_four;
      done = false;
    }else if(i == 5 && rain_data.at(5) > 0){
      rain_data.at(5) -= 1;
      m_water_squish += m_rain_five;
      done = false;
    }
  }
  //reset rain stuff when all clouds are removed and all rain has stopped
  if(done){
    rain_time = false;
    cloud_timer = 0;
    rain_timer = 0;
  }
}

void WaterSimLayer::addClouds(){
  m_clouds_squished.setTo(0);
  for(unsigned int i = 0; i < rain_data.size(); i++){
    if(i == 1 && rain_data.at(1) > 0){
      m_clouds_squished += m_rain_one;
    }else if(i == 2 && rain_data.at(2) > 0){
      m_clouds_squished += m_rain_two;
    }else if(i == 3 && rain_data.at(3) > 0){
      m_clouds_squished += m_rain_three;
    }else if(i == 4 && rain_data.at(4) > 0){
      m_clouds_squished += m_rain_four;
    }else if(i == 5 && rain_data.at(5) > 0){
      m_clouds_squished += m_rain_five;
    }
  }
  cv::resize(m_clouds_squished, m_clouds, m_clouds.size(), 0, 0);
  emit materialPropertyChanged(Clouds);
}

void WaterSimLayer::removeClouds(){
  for(unsigned int i = 0; i < rain_data.size(); i++){
    if(i == 1 && rain_data.at(1) == 1){
      m_clouds_squished -= m_rain_one;
      m_clouds_squished.setTo(0, m_clouds_squished < 1);
      cv::resize(m_clouds_squished, m_clouds, m_clouds.size(), 0, 0);
      emit materialPropertyChanged(Clouds);
    }else if(i == 2 && rain_data.at(2) == 1){
      m_clouds_squished -= m_rain_two;
      m_clouds_squished.setTo(0, m_clouds_squished < 1);
      cv::resize(m_clouds_squished, m_clouds, m_clouds.size(), 0, 0);
      emit materialPropertyChanged(Clouds);
    }else if(i == 3 && rain_data.at(3) == 1){
      m_clouds_squished -= m_rain_three;
      m_clouds_squished.setTo(0, m_clouds_squished < 1);
      cv::resize(m_clouds_squished, m_clouds, m_clouds.size(), 0, 0);
      emit materialPropertyChanged(Clouds);
    }else if(i == 4 && rain_data.at(4) == 1){
      m_clouds_squished -= m_rain_four;
      m_clouds_squished.setTo(0, m_clouds_squished < 1);
      cv::resize(m_clouds_squished, m_clouds, m_clouds.size(), 0, 0);
      emit materialPropertyChanged(Clouds);
    }else if(i == 5 && rain_data.at(5) == 1){
      m_clouds_squished -= m_rain_five;
      m_clouds_squished.setTo(0, m_clouds_squished < 1);
      cv::resize(m_clouds_squished, m_clouds, m_clouds.size(), 0, 0);
      emit materialPropertyChanged(Clouds);
    }
  }
}

void WaterSimLayer::spread(int y, int x, float depth, float height){
  if(area_counter < max_area && m_depth_mod.at<float>(y, x) > height-depth && m_depth_mod.at<float>(y, x) > 0 && m_depth_mod.at<float>(y, x) < 125){
    m_depth_mod.at<float>(y, x) = 1000;
    m_current_spread.at<float>(y, x) = 1;
    area_counter++;
    spread(y, x-1, depth, height);
    spread(y-1, x, depth, height);
    spread(y, x+1, depth, height);
    spread(y+1, x, depth, height);
  }
}

void WaterSimLayer::decimateRain(int rain_counter){
  if(rain_counter == 1){
    for(int o = y_low_edge/5; o < m_rain_one.rows - y_high_edge/5; o++){
      for(int p = x_low_edge/5; p < m_rain_one.cols - x_high_edge/5; p++){
        if(o%8 < 6 || p%8 < 6){
          m_rain_one.at<float>(o, p) = 0;
        }
      }
    }
  }else if(rain_counter == 2){
    for(int o = y_low_edge/5; o < m_rain_one.rows - y_high_edge/5; o++){
      for(int p = x_low_edge/5; p < m_rain_one.cols - x_high_edge/5; p++){
        if(o%8 < 6 || p%8 < 6){
          m_rain_two.at<float>(o, p) = 0;
        }
      }
    }
  }else if(rain_counter == 3){
    for(int o = y_low_edge/5; o < m_rain_one.rows - y_high_edge/5; o++){
      for(int p = x_low_edge/5; p < m_rain_one.cols - x_high_edge/5; p++){
        if(o%8 < 6 || p%8 < 6){
          m_rain_three.at<float>(o, p) = 0;
        }
      }
    }
  }else if(rain_counter == 4){
    for(int o = y_low_edge/5; o < m_rain_one.rows - y_high_edge/5; o++){
      for(int p = x_low_edge/5; p < m_rain_one.cols - x_high_edge/5; p++){
        if(o%8 < 6 || p%8 < 6){
          m_rain_four.at<float>(o, p) = 0;
        }
      }
    }
  }else{
    for(int o = y_low_edge/5; o < m_rain_one.rows - y_high_edge/5; o++){
      for(int p = x_low_edge/5; p < m_rain_one.cols - x_high_edge/5; p++){
        if(o%8 < 6 || p%8 < 6){
          m_rain_five.at<float>(o, p) = 0;
        }
      }
    }
  }
}

void WaterSimLayer::fixRain(int rain_counter){
  if(rain_counter == 1){
    for(int o = y_low_edge/5; o < m_rain_one.rows - y_high_edge/5; o++){
      for(int p = x_low_edge/5; p < m_rain_one.cols - x_high_edge/5; p++){
        if(!o%8 < 6 && !p%8 < 6 && m_rain_one.at<float>(o, p) > 0.1){
          if(!(o-1)%8 < 6 && !(p-1)%8 < 6){
            m_rain_one.at<float>(o-1, p-1) = 1;
            m_rain_one.at<float>(o-1, p) = 1;
            m_rain_one.at<float>(o, p-1) = 1;
          }else if(!(o-1)%8 < 6 && !(p+1)%8 < 6){
            m_rain_one.at<float>(o-1, p+1) = 1;
            m_rain_one.at<float>(o-1, p) = 1;
            m_rain_one.at<float>(o, p+1) = 1;
          }else if(!(o+1)%8 < 6 && !(p+1)%8 < 6){
            m_rain_one.at<float>(o+1, p+1) = 1;
            m_rain_one.at<float>(o+1, p) = 1;
            m_rain_one.at<float>(o, p+1) = 1;
          }else{
            m_rain_one.at<float>(o+1, p-1) = 1;
            m_rain_one.at<float>(o+1, p) = 1;
            m_rain_one.at<float>(o, p-1) = 1;
          }
        }
      }
    }
  }else if(rain_counter == 2){
    for(int o = y_low_edge/5; o < m_rain_one.rows - y_high_edge/5; o++){
      for(int p = x_low_edge/5; p < m_rain_one.cols - x_high_edge/5; p++){
        if(!o%8 < 6 && !p%8 < 6 && m_rain_two.at<float>(o, p) > 0.1){
          if(!(o-1)%8 < 6 && !(p-1)%8 < 6){
            m_rain_two.at<float>(o-1, p-1) = 1;
            m_rain_two.at<float>(o-1, p) = 1;
            m_rain_two.at<float>(o, p-1) = 1;
          }else if(!(o-1)%8 < 6 && !(p+1)%8 < 6){
            m_rain_two.at<float>(o-1, p+1) = 1;
            m_rain_two.at<float>(o-1, p) = 1;
            m_rain_two.at<float>(o, p+1) = 1;
          }else if(!(o+1)%8 < 6 && !(p+1)%8 < 6){
            m_rain_two.at<float>(o+1, p+1) = 1;
            m_rain_two.at<float>(o+1, p) = 1;
            m_rain_two.at<float>(o, p+1) = 1;
          }else{
            m_rain_two.at<float>(o+1, p-1) = 1;
            m_rain_two.at<float>(o+1, p) = 1;
            m_rain_two.at<float>(o, p-1) = 1;
          }
        }
      }
    }
  }else if(rain_counter == 3){
    for(int o = y_low_edge/5; o < m_rain_one.rows - y_high_edge/5; o++){
      for(int p = x_low_edge/5; p < m_rain_one.cols - x_high_edge/5; p++){
        if(!o%8 < 6 && !p%8 < 6 && m_rain_three.at<float>(o, p) > 0.1){
          if(!(o-1)%8 < 6 && !(p-1)%8 < 6){
            m_rain_three.at<float>(o-1, p-1) = 1;
            m_rain_three.at<float>(o-1, p) = 1;
            m_rain_three.at<float>(o, p-1) = 1;
          }else if(!(o-1)%8 < 6 && !(p+1)%8 < 6){
            m_rain_three.at<float>(o-1, p+1) = 1;
            m_rain_three.at<float>(o-1, p) = 1;
            m_rain_three.at<float>(o, p+1) = 1;
          }else if(!(o+1)%8 < 6 && !(p+1)%8 < 6){
            m_rain_three.at<float>(o+1, p+1) = 1;
            m_rain_three.at<float>(o+1, p) = 1;
            m_rain_three.at<float>(o, p+1) = 1;
          }else{
            m_rain_three.at<float>(o+1, p-1) = 1;
            m_rain_three.at<float>(o+1, p) = 1;
            m_rain_three.at<float>(o, p-1) = 1;
          }
        }
      }
    }
  }else if(rain_counter == 4){
    for(int o = y_low_edge/5; o < m_rain_one.rows - y_high_edge/5; o++){
      for(int p = x_low_edge/5; p < m_rain_one.cols - x_high_edge/5; p++){
        if(!o%8 < 6 && !p%8 < 6 && m_rain_four.at<float>(o, p) > 0.1){
          if(!(o-1)%8 < 6 && !(p-1)%8 < 6){
            m_rain_four.at<float>(o-1, p-1) = 1;
            m_rain_four.at<float>(o-1, p) = 1;
            m_rain_four.at<float>(o, p-1) = 1;
          }else if(!(o-1)%8 < 6 && !(p+1)%8 < 6){
            m_rain_four.at<float>(o-1, p+1) = 1;
            m_rain_four.at<float>(o-1, p) = 1;
            m_rain_four.at<float>(o, p+1) = 1;
          }else if(!(o+1)%8 < 6 && !(p+1)%8 < 6){
            m_rain_four.at<float>(o+1, p+1) = 1;
            m_rain_four.at<float>(o+1, p) = 1;
            m_rain_four.at<float>(o, p+1) = 1;
          }else{
            m_rain_four.at<float>(o+1, p-1) = 1;
            m_rain_four.at<float>(o+1, p) = 1;
            m_rain_four.at<float>(o, p-1) = 1;
          }
        }
      }
    }
  }else{
    for(int o = y_low_edge/5; o < m_rain_one.rows - y_high_edge/5; o++){
      for(int p = x_low_edge/5; p < m_rain_one.cols - x_high_edge/5; p++){
        if(!o%8 < 6 && !p%8 < 6 && m_rain_five.at<float>(o, p) > 0.1){
          if(!(o-1)%8 < 6 && !(p-1)%8 < 6){
            m_rain_five.at<float>(o-1, p-1) = 1;
            m_rain_five.at<float>(o-1, p) = 1;
            m_rain_five.at<float>(o, p-1) = 1;
          }else if(!(o-1)%8 < 6 && !(p+1)%8 < 6){
            m_rain_five.at<float>(o-1, p+1) = 1;
            m_rain_five.at<float>(o-1, p) = 1;
            m_rain_five.at<float>(o, p+1) = 1;
          }else if(!(o+1)%8 < 6 && !(p+1)%8 < 6){
            m_rain_five.at<float>(o+1, p+1) = 1;
            m_rain_five.at<float>(o+1, p) = 1;
            m_rain_five.at<float>(o, p+1) = 1;
          }else{
            m_rain_five.at<float>(o+1, p-1) = 1;
            m_rain_five.at<float>(o+1, p) = 1;
            m_rain_five.at<float>(o, p-1) = 1;
          }
        }
      }
    }
  }
}


void WaterSimLayer::rainTriggered(){
  // if(!rain_time && !start_rain){
    rain_timer = 0;
    start_rain = true;
    cloud_timer = 0;
  // }
}


void WaterSimLayer::waterRemoved(){
  m_water_squish.setTo(0);
  emit materialPropertyChanged(Water);
  m_clouds.setTo(0);
  emit materialPropertyChanged(Clouds);
  rain_time = false;
  cloud_timer = 0;
}