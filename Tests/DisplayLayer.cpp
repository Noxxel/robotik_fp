#include <QtCore/QDebug>

#include "Application/PixmapBuffer.h"

#include "DisplayLayer.h"

DisplayLayer::DisplayLayer(Application *app)
  : Layer(app),
    m_buf(this, PixmapBuffer::RGBA),
    m_buf2(this), m_buf2Progress(0.0)
{

  m_depth = this->createPropertyBuffer();

    qDebug() << "DisplayLayer::DisplayLayer(): " << this->fieldSize();
  /* resize the buffers in order to use them */
  m_buf.setSize(this->fieldSize());
  m_buf2.setSize(QSize(30, 30));

  this->startTimer(100);

}

void DisplayLayer::render()
{
  /* render a rectangle in red */
/*  glColor3d(1,0,0);
  glBegin(GL_QUADS);
  glVertex2d(-1, -1);
  glVertex2d(1, -1);
  glVertex2d(1, 1);
  glVertex2d(-1, 1);
  glEnd();*/

  /* finally render both buffers */
  m_buf.render();
  //m_buf2.render();
}

void DisplayLayer::prepareRender(const QRectF &region)
{


  /* ## here we are going to reduce the physical system to a
   * ## visual representation. Everything that is above 2°C
   * ## is molten water, anything below is ice. */

  /* define scalars that will be used to colorize our pixmap */
  cv::Scalar green(0.0, 1.0, 0.0, 1);
  cv::Scalar white(1.0, 1.0, 1.0, 1);
  cv::Scalar red(1.0, 0.0, 0.0, 1);
  cv::Scalar blue(0.0, 0.0, 1.0, 1);

  /* get a shortcut to the temperature property */
  const cv::Mat T = this->materialProperty(Temperature);

  /* initially set everything to blue */
  m_buf.buffer() = blue;

  for(int i=0; i<20; i++){
    cv::Vec<double, 4> temp(((i+1)/20.0), (1.0-((i+1)/20.0)), 0.0, 1.0);
    // std::cout<<temp<<std::endl;
    m_buf.buffer().setTo(temp, T>(((i*0.5)*100)+10));
  // for(int i=0; i<15; i++){
  //   cv::Scalar temp(((i+1)/15.0), (1.0-((i+1)/15.0)), 0.0, 1.0);
  //   m_buf.buffer().setTo(temp, m_depth>(((i)*7)-10));
  }

  // for(int y=0; y<m_buf.buffer().rows; y++){
  //   for(int x=0; x<m_buf.buffer().rows; x++){
  //     float temp = m_depth.at<float>(y,x);
  //     // std::cout<<x<<"    "<<y<<std::endl;
  //     // std::cout<<m_buf.buffer().rows<<"     "<<m_buf.buffer().cols<<std::endl;
  //     // std::cout<<m_depth.rows<<"     "<<m_depth.cols<<std::endl;
  //     m_buf.buffer().at<cv::Scalar>(y,x) = (temp/100.0)*red + (1-(temp/100.0))*green;
  //     // m_buf.buffer().at<int>(y,x,1) = (temp/100.0)*255;
  //     // m_buf.buffer().at<int>(y,x,2) = (1-(temp/100))*255;
  //   }
  // }

  /* set the render position */
  m_buf.setViewport(QRectF(-1, -1, 2, 2));

  /* ## create a second pixmap with a cross */

  /* set the render viewport to the lower left quadrant */
  // QRectF buf2Viewport(1, 1, 0.5, 0.5);
  // buf2Viewport.moveCenter(m_buf2Pos);
  // m_buf2.setViewport(buf2Viewport);
  // m_buf2.setOpacity(0.7);


  // /* set the buffer to black and colorize some rows and columns */
  // m_buf2.buffer() = cv::Scalar(0.0, 0.0, 0.0);
  // m_buf2.buffer().row(10) = cv::Scalar(1.0, 0.5, 1.0);
  // m_buf2.buffer().row(11) = cv::Scalar(1.0, 0.5, 1.0);
  // m_buf2.buffer().row(12) = cv::Scalar(1.0, 0.5, 1.0);
  // m_buf2.buffer().col(10) = cv::Scalar(1.0, 0.5, 1.0);
  // m_buf2.buffer().col(11) = cv::Scalar(1.0, 0.5, 1.0);


}

void DisplayLayer::update(Layer::PropertyTypes t, QRect rect)
{
  /* check if temperature has changed */
  if(t & Temperature) {
    emit appearanceChanged(rect);
  }
}

void DisplayLayer::timerEvent(QTimerEvent *event)
{
  m_buf2Progress += 0.01;

  qreal angle = m_buf2Progress*2.0*M_PI;

  m_buf2Pos = QPointF(0.25*sin(angle), 0.25*cos(angle));
  m_buf2Pos -= QPointF(0.25, 0.25);

  /* signal the change */
  emit appearanceChanged();

}

void DisplayLayer::updateDepth(QRect region)
{

  /* set the height */
  m_depth = depth();
}