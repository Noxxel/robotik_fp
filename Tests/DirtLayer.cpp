#include <QtCore/QDebug>

#include "Application/PixmapBuffer.h"

#include "DirtLayer.h"

DirtLayer::DirtLayer(Application *app)
  : Layer(app),
    m_buf(this, PixmapBuffer::RGBA)
{


    qDebug() << "DirtLayer::DirtLayer(): " << this->fieldSize();
  /* resize the buffers in order to use them */
  m_buf.setSize(this->fieldSize());

  m_fert = this -> createPropertyBuffer();
  m_dirt = this -> createPropertyBuffer();
  m_dirt_bot = this -> createPropertyBuffer();
  m_dirt_top = this -> createPropertyBuffer();

  this->startTimer(100);

}

void DirtLayer::render()
{

  /* finally render both buffers */
  m_buf.render();
}

void DirtLayer::prepareRender(const QRectF &region)
{


  /* ## here we are going to reduce the physical system to a
   * ## visual representation. Everything that is above 2°C
   * ## is molten water, anything below is ice. */

  /* define scalars that will be used to colorize our pixmap */
  cv::Scalar transp(0.0, 0.0, 0.0, 0);
  cv::Scalar dirtcolor(0.80, 0.30, 0.25, 1);

  /* set the buffer to dirt everywhere above groundwater */

  m_buf.buffer() = transp;
  m_buf.buffer().setTo(dirtcolor, m_dirt == 1);

  // m_fert.setTo(1, m_depth>=8);
  // std::cout << "Fertility: " << m_fert;

  /* set the render position */
  m_buf.setViewport(QRectF(-1, -1, 2, 2));

}

void DirtLayer::update(Layer::PropertyTypes t, QRect rect)
{
  if(t & Fertility){
    Layer::materialProperty(Fertility).copyTo(m_fert);
    m_fert.setTo(1, m_dirt == 1);
    m_fert.setTo(0, Layer::materialProperty(Water) >= 1);
  }

  /* check if temperature has changed */
  // if(t & Temperature) {
  //     emit appearanceChanged(rect);
  //   }
}


const cv::Mat DirtLayer::materialProperty(PropertyType t)
{

  if(t == Layer::Fertility)
    return m_fert;

  /* for anything else we don't care */  
  return Layer::materialProperty(t);
}

void DirtLayer::timerEvent(QTimerEvent *event)
{
  // m_buf2Progress += 0.01;

  // qreal angle = m_buf2Progress*2.0*M_PI;

  // m_buf2Pos = QPointF(0.25*sin(angle), 0.25*cos(angle));
  // m_buf2Pos -= QPointF(0.25, 0.25);

  /* signal the change */
  // emit appearanceChanged();

}

void DirtLayer::updateDepth(QRect region)
{

  /* set the height */
  m_depth = depth();

  m_dirt_bot.setTo(0);
  m_dirt_top.setTo(0);
  m_dirt.setTo(0);

  m_dirt_bot.setTo(1, m_depth >= 5);
  m_dirt_top.setTo(1, m_depth <= 80);

  cv::multiply(m_dirt_bot, m_dirt_top, m_dirt);
  emit appearanceChanged();
  emit materialPropertyChanged(Fertility);
}
