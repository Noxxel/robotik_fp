#include <QtCore/QDebug>

#include "Application/PixmapBuffer.h"

#include "CloudLayer.h"

CloudLayer::CloudLayer(Application *app)
  : Layer(app),
    m_buf(this, PixmapBuffer::RGBA)
{


    qDebug() << "CloudLayer::CloudLayer(): " << this->fieldSize();
  /* resize the buffers in order to use them */
  m_buf.setSize(this->fieldSize());
  m_clouds = this -> createPropertyBuffer();
  m_clouds_temp = this -> createPropertyBuffer();
  m_clouds_last = this -> createPropertyBuffer();
  m_clouds_change = this -> createPropertyBuffer();
  m_clouds_render = cv::Mat(m_clouds.size().height, m_clouds.size().width,
                   CV_32FC4, 0.0);

  m_cloud_color = this -> createPropertyBuffer();
  m_cloud_color.setTo(0.35);
  m_cloud_channel4 = this -> createPropertyBuffer();
  // m_clouds_squish = this -> createPropertyBufferSquish();

  // m_clouds_temp = this -> createPropertyBuffer();

  m_clouds.setTo(0);
  m_cloud_channel4.setTo(0);
  transition_in_progress = 50;

  this->startTimer(150);

}

void CloudLayer::render()
{

  /* finally render both buffers */
  m_buf.render();
}

void CloudLayer::prepareRender(const QRectF &region)
{


  /* ## here we are going to reduce the physical system to a
   * ## visual representation. Everything that is above 2°C
   * ## is molten water, anything below is ice. */

  /* define scalars that will be used to colorize our pixmap */
  const cv::Scalar transp(0.0, 0.0, 0.0, 0.0);

  /* get a shortcut to the temperature property */
  // const cv::Mat H = this->materialProperty(Humidity);

  /* initially set everything to white */
  m_buf.buffer() = transp;

  /* set the buffer to blue everywhere it is warm enough */
  m_clouds_render.copyTo(m_buf.buffer());
  // m_buf.buffer().setTo(sand, H >= 0.75);
  // m_buf.buffer().setTo(blue1, W >= 0.1);
  // std::cout<<cv::Mat(m_clouds, cv::Rect(620, 350, 10, 10))<<std::endl;
  // m_buf.buffer().setTo(blue2, m_clouds >= 1);
  // m_buf.buffer().setTo(blue3, m_clouds >= 15);
  // m_buf.buffer().setTo(blue4, m_clouds >= 30);

  /* set the render position */
  m_buf.setViewport(QRectF(-1, -1, 2, 2));

}

void CloudLayer::update(Layer::PropertyTypes t, QRect rect)
{
  /* check if temperature has changed */
  if(t & Clouds) {


    m_cloud_channel4.copyTo(m_clouds_last);
    Layer::materialProperty(Clouds).copyTo(m_clouds_temp);


    double min,max;
    cv::minMaxLoc(m_clouds_temp, &min, &max);
    if(max > 0){
      m_clouds_temp /= max;
    }
    m_clouds_temp.copyTo(m_clouds);

    for(int i = 0; i < 10; i++){
      m_clouds_temp.copyTo(m_clouds, m_clouds_temp >= 1);
      cv::GaussianBlur(m_clouds, m_clouds, cv::Size(21,21), 25.0);
      m_clouds /= 0.5;
    }
    m_clouds_temp.copyTo(m_clouds, m_clouds_temp >= 1);
    m_clouds.setTo(1, m_clouds > 1);
    cv::GaussianBlur(m_clouds, m_clouds, cv::Size(45,45), 45.0);
    m_clouds.setTo(0, m_clouds < 0);

    m_clouds_change.setTo(0);
    m_clouds_change = m_clouds - m_clouds_last;
    m_clouds_change /= 30;

    transition_in_progress = 0;
    // std::cout<<cv::Mat(m_clouds, cv::Rect(315, 235, 10, 10))<<std::endl;



  }

  // if(t & Humidity)
    // emit appearanceChanged(rect);
}

const cv::Mat CloudLayer::materialProperty(PropertyType t)
{
  if(t == Layer::RenderedClouds)
    return m_cloud_channel4;

  /* for anything else we don't care */
  return Layer::materialProperty(t);
}

//add depthFunction 


void CloudLayer::timerEvent(QTimerEvent *event)
{
  cv::randn(m_cloud_color, 8, 0.75);
  double min,max;
  cv::minMaxLoc(m_cloud_color, &min, &max);
  m_cloud_color /= max*2;
  m_cloud_color.setTo(0.2, m_cloud_color < 0.2);
  cv::blur(m_cloud_color, m_cloud_color, cv::Size(9, 9));


  channel_mats.clear();
  //red
  channel_mats.push_back(m_cloud_color);

  //green
  channel_mats.push_back(m_cloud_color);

  //blue
  channel_mats.push_back(m_cloud_color);

  if(transition_in_progress < 30){
    //alpha
    m_cloud_channel4 += m_clouds_change;
    m_clouds.setTo(0, m_clouds < 0);
    m_cloud_channel4.setTo(0, m_cloud_channel4 < 0);

    transition_in_progress++;
  }

  channel_mats.push_back(m_cloud_channel4);

  cv::merge(channel_mats, m_clouds_render);
  emit materialPropertyChanged(RenderedClouds);
  emit appearanceChanged();
}

