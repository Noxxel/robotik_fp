#include <QtCore/QDebug>
#include <QtCore/QTimer>
#include <QtCore/QTime>

#include "Application/PixmapBuffer.h"

#include "VegetationLayer.h"

VegetationLayer::VegetationLayer(Application *app)
  : Layer(app),
    m_buf(this, PixmapBuffer::RGBA)
{


  qDebug() << "VegetationLayer::VegetationLayer(): " << this->fieldSize();

  /* resize the buffers in order to use them */
  m_buf.setSize(this->fieldSize());

  m_depth = this -> createPropertyBuffer();
  m_depth_last = this -> createPropertyBuffer();
  m_depth_changed = this -> createPropertyBuffer();
  m_depth_relevant = this -> createPropertyBuffer();
  m_fert = this -> createPropertyBuffer();
  m_humid = this -> createPropertyBuffer();
  m_vegetation = this -> createPropertyBuffer();
  m_vegetation.setTo(0);
  m_temperature = this -> createPropertyBuffer();
  m_temperature_mod = this -> createPropertyBuffer();
  m_incr = this -> createPropertyBuffer();
  m_decay = this -> createPropertyBuffer();
  m_vegetation_channel1 = this -> createPropertyBuffer();
  m_vegetation_channel2 = this -> createPropertyBuffer();
  m_vegetation_channel3 = this -> createPropertyBuffer();
  m_vegetation_channel4 = this -> createPropertyBuffer();
  m_vegetation_render = cv::Mat(m_depth.size().height, m_depth.size().width,
                   CV_32FC4, 0.0);
  
  m_depth.setTo(0);
  this->startTimer(200);

}

void VegetationLayer::render()
{
  /* finally render both buffers */
  m_buf.render();
}

void VegetationLayer::prepareRender(const QRectF &region)
{

  /* define scalars that will be used to colorize our pixmap */
  cv::Scalar transp(0.0, 0.0, 0.0, 0);

  /* set the buffer to green wherever we fertility and enough humidity */
  m_buf.buffer() = transp;

  m_vegetation_render.copyTo(m_buf.buffer());

  // for(int i = 3; i <= 10; i++){
  //   cv::Vec<double, 4> temp(0.2 - i*0.02, 0.55 - i*0.03, 0.15 - i*0.04, 0.35+i*0.065);
  //   m_buf.buffer().setTo(temp, m_vegetation > 0.025 +(i-3)*0.06);
  // }

  /* set the render position */
  m_buf.setViewport(QRectF(-1, -1, 2, 2));

  //min: (0.14, 0.50, 0.03, 0.1)
  //max: (0, 0.25, 0, 1)
}

void VegetationLayer::update(Layer::PropertyTypes t, QRect rect)
{
  if(t &Fertility) {
    Layer::materialProperty(Fertility).copyTo(m_fert);
    // emit appearanceChanged(rect);
  }
  if(t &Humidity) {
    Layer::materialProperty(Humidity).copyTo(m_humid);
    // emit appearanceChanged(rect);
  }
  if(t &Temperature){
    Layer::materialProperty(Temperature).copyTo(m_temperature);
  }
  if(t &Vegetation){
    // QTime timer;
    // timer.start();

    m_vegetation.copyTo(m_test_veg);

    m_humid.copyTo(m_incr);
    m_incr *= 0.004;

    m_temperature.copyTo(m_temperature_mod, m_temperature <= 15);
    m_temperature_mod.setTo(0, m_temperature_mod < 0);
    m_temperature_mod /= 15;
    m_temperature_mod.setTo(1, m_temperature > 15);

    m_test_veg += m_incr;
    m_incr.setTo(0, m_test_veg > m_humid);
    m_incr.setTo(0, m_test_veg > m_temperature_mod);

    m_humid.copyTo(m_decay);
    m_decay *= -decay;
    m_decay += decay;
    m_decay.copyTo(m_incr, m_vegetation > m_humid);

    m_temperature.copyTo(m_decay, m_temperature <= 15);
    m_decay *= (-1/15)*decay;
    m_decay += decay;
    m_temperature_mod.setTo(1, m_temperature > 15);
    m_decay.copyTo(m_incr, m_vegetation > m_temperature_mod);

    m_vegetation += m_incr;

    m_vegetation.setTo(0, m_vegetation < 0);
    m_vegetation.setTo(1, m_vegetation > 1);
    m_vegetation.setTo(0, m_fert == 0);

    channel_mats.clear();

    //red
    m_vegetation.copyTo(m_vegetation_channel1);
    m_vegetation_channel1 *= 1.2;
    m_vegetation_channel1 *= -(1/6.5);
    m_vegetation_channel1 += (1/6.5);
    channel_mats.push_back(m_vegetation_channel1);

    //green
    m_vegetation.copyTo(m_vegetation_channel2);
    m_vegetation_channel2 *= 1.2;
    m_vegetation_channel2 *= -0.35;
    m_vegetation_channel2 += 0.6;
    channel_mats.push_back(m_vegetation_channel2);

    //blue
    m_vegetation_channel3.setTo(0);
    channel_mats.push_back(m_vegetation_channel3);

    //alpha
    m_vegetation.copyTo(m_vegetation_channel4);
    m_vegetation_channel4 *= 7;
    m_vegetation_channel4.setTo(1, m_vegetation_channel4 > 1);
    channel_mats.push_back(m_vegetation_channel4);

    cv::merge(channel_mats, m_vegetation_render);

    emit appearanceChanged();

    // qDebug() << "\n (" << timer.elapsed() << " ms)";
  }
}


const cv::Mat VegetationLayer::materialProperty(PropertyType t)
{
  
  // if(t == Layer::Humidity)
  //   return m_humid;

  // if(t == Layer::Fertility)
  //   return m_fert;

  if(t == Layer::Vegetation)
    return m_vegetation;
  
  /* for anything else we don't care */
  return Layer::materialProperty(t);
}


void VegetationLayer::timerEvent(QTimerEvent *event)
{

  emit materialPropertyChanged(Vegetation);

}

void VegetationLayer::updateDepth(QRect region)
{

  /* set the height */
  m_depth.copyTo(m_depth_last);
  depth().copyTo(m_depth);
  m_depth_changed = m_depth_last - m_depth;
  m_depth_relevant.setTo(0);
  m_depth_relevant.setTo(1, m_depth_changed > 3);
  m_depth_relevant.setTo(1, m_depth_changed < -3);

  m_vegetation.setTo(0, m_depth_relevant == 1);

  emit appearanceChanged();

}
