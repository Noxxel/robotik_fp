#ifndef VEGETATIONLAYER_H
#define VEGETATIONLAYER_H


#include "Layer/Layer.h"
#include "Application/PixmapBuffer.h"

class VegetationLayer : public Layer
{
  Q_OBJECT

  /* create two pixmap buffers,
   * one will have opacity channel, the other
   * don't. */
  PixmapBuffer m_buf;
  cv::Mat m_depth;
  cv::Mat m_depth_last;
  cv::Mat m_depth_changed;
  cv::Mat m_depth_relevant;
  cv::Mat m_fert;
  cv::Mat m_humid;
  cv::Mat m_vegetation;
  cv::Mat m_vegetation_channel1;
  cv::Mat m_vegetation_channel2;
  cv::Mat m_vegetation_channel3;
  cv::Mat m_vegetation_channel4;
  cv::Mat m_vegetation_render;
  cv::Mat m_temperature;
  cv::Mat m_temperature_mod;
  cv::Mat m_incr;
  cv::Mat m_test_veg;
  cv::Mat m_decay;
  std::vector<cv::Mat> channel_mats;
  float decay{-0.025};

public:
  explicit VegetationLayer(Application * app);

  const cv::Mat materialProperty(PropertyType t);

  void render();
  void prepareRender(const QRectF & region);

  void update(PropertyTypes t, QRect rect);
  void updateDepth(QRect region);
  
signals:

public slots:

protected:
  void timerEvent(QTimerEvent * event);

};

#endif // VEGETATIONLAYER_H
