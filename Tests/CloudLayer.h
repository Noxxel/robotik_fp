#ifndef CLOUDLAYER_H
#define CLOUDLAYER_H


#include "Layer/Layer.h"
#include "Application/PixmapBuffer.h"

class CloudLayer : public Layer
{
  Q_OBJECT

  int transition_in_progress;
  cv::Mat m_clouds;
  cv::Mat m_clouds_temp;
  cv::Mat m_clouds_last;
  cv::Mat m_clouds_render;
  cv::Mat m_clouds_change;
  std::vector<cv::Mat> channel_mats;

  cv::Mat m_cloud_color;
  cv::Mat m_cloud_channel4;

  // cv::Mat m_water_squish;
  // cv::Mat m_water_temp;

  /* create two pixmap buffers,
   * one will have opacity channel, the other
   * don't. */
  PixmapBuffer m_buf;

public:
  explicit CloudLayer(Application * app);

  const cv::Mat materialProperty(PropertyType t);
  
  void render();
  void prepareRender(const QRectF & region);

  void update(PropertyTypes t, QRect rect);
signals:

public slots:

protected:
  void timerEvent(QTimerEvent * event);

};

#endif // CLOUDLAYER_H
