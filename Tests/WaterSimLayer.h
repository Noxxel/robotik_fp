#ifndef WATERSIMLAYER
#define WATERSIMLAYER


#include "../Layer/Layer.h"
#include "Application/PixmapBuffer.h"
#include <fstream>

class WaterSimLayer : public Layer
{
  Q_OBJECT

  /* create two pixmap buffers,
   * one will have opacity channel, the other
   * don't. */
  // PixmapBuffer m_buf;
  cv::Mat m_depth;
  cv::Mat m_depth_squish;
  cv::Mat m_water;
  cv::Mat m_water_squish;
  cv::Mat m_water_static;
  cv::Mat m_fert;
  cv::Mat m_humid;
  cv::Mat m_temperature;
  cv::Mat m_humid_temp;
  std::vector<std::vector<float>> v_spread_data;
  cv::Mat m_evaporate;
  cv::Mat m_evaporate_humidity;
  std::vector<float> temp;
  std::vector<float> v_val;
  std::vector<float> v_loc;
  int var_y;
  int var_x;
  bool start_rain;
  bool rain_time;

  int rain_timer;
  cv::Mat m_rain_one;
  cv::Mat m_rain_two;
  cv::Mat m_rain_three;
  cv::Mat m_rain_four;
  cv::Mat m_rain_five;
  cv::Mat m_temp_rain;

  int cloud_timer;
  cv::Mat m_clouds;
  cv::Mat m_clouds_squished;

  cv::Mat m_depth_last;

  cv::Mat m_current_spread;
  int area_counter;
  cv::Mat m_depth_mod;
  std::vector<int> rain_data;

  float sum;
  cv::Scalar s_sum;

  int max_area{2500};
  float max_water{50000};
  float non_diagonal_support{1.02};
  int t_progress = 1;

  int x_low_edge{145};
  int x_high_edge{150};
  int y_low_edge{25};
  int y_high_edge{105};

public:
  explicit WaterSimLayer(Application * app);

  const cv::Mat materialProperty(PropertyType t);
  
  void render();
  void prepareRender(const QRectF & region);

  void update(PropertyTypes t, QRect rect);
  void updateDepth(QRect region);
signals:

public slots:
  void rainTriggered();
  void waterRemoved();

protected:
  void timerEvent(QTimerEvent * event);

  void findWater();
  void spreadWater(int looking_at);
  void spreadWaterLast(int looking_at);

  void addRain();
  void addWater();
  void spread(int y, int x, float depth, float height);
  void decimateRain(int rain_counter);
  void fixRain(int rain_counter);

  void addClouds();
  void removeClouds();

};

#endif // WATERSIMLAYER
