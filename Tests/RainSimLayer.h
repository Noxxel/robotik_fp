#ifndef RAINSIMLAYER
#define RAINSIMLAYER

#include "Layer/Layer.h"

class RainSimLayer : public Layer
{
  Q_OBJECT

  /* this layer redefines the temperature, so we have to hold
   * a buffer for it, that can be used by materialProperty() */
  cv::Mat m_depth;

public:
  explicit RainSimLayer(Application *app);


  const cv::Mat materialProperty(PropertyType t);
signals:

public slots:

  void update(PropertyTypes, QRect rect);
  void updateDepth(QRect region);

protected:
  void timerEvent(QTimerEvent * event);

};

#endif // RAINSIMLAYER
