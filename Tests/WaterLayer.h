#ifndef WATERLAYER_H
#define WATERLAYER_H


#include "Layer/Layer.h"
#include "Application/PixmapBuffer.h"

class WaterLayer : public Layer
{
  Q_OBJECT

  cv::Mat m_water;
  cv::Mat m_water_render;
  std::vector<cv::Mat> channel_mats;

  cv::Mat m_water_channel1;
  cv::Mat m_water_channel2;
  cv::Mat m_water_channel3;
  cv::Mat m_water_channel4;

  // cv::Mat m_water_squish;
  // cv::Mat m_water_temp;

  /* create two pixmap buffers,
   * one will have opacity channel, the other
   * don't. */
  PixmapBuffer m_buf;

public:
  explicit WaterLayer(Application * app);

  const cv::Mat materialProperty(PropertyType t);
  
  void render();
  void prepareRender(const QRectF & region);

  void update(PropertyTypes t, QRect rect);
signals:

public slots:

protected:
  void timerEvent(QTimerEvent * event);

};

#endif // WATERLAYER_H
