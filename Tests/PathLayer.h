#ifndef PATHLAYER_H
#define PATHLAYER_H


#include "Layer/Layer.h"
#include "Application/PixmapBuffer.h"
#include <iostream>
#include <fstream>
#include <unordered_set>

class PathLayer : public Layer
{
  Q_OBJECT


  struct Node{
    int y;
    int x;
    float cost;
    float distance;
    float priority;
    int parent_y;
    int parent_x;


    // Node(int xpos, int ypos){
    //   x = xpos;
    //   y = ypos;
    // }

    friend bool operator< (const Node &n1, const Node &n2){ 
      return n1.priority > n2.priority;
    }

    friend bool operator== (const Node &n1, const Node &n2){
      return (n1.y == n2.y && n1.x == n2.x);
    }

    friend std::ostream &operator<< (std::ostream &ostr, const Node &n1){
      ostr << "x-coord: " << n1.x;
      ostr << "   y-coord: " << n1.y <<std::endl;
      ostr << "priority: " << n1.priority <<::std::endl;
      ostr << "costs: "<<n1.cost<<std::endl;
      ostr << "parent_x: "<<n1.parent_x<<std::endl;
      ostr << "parent_y: "<<n1.parent_y<<std::endl;
      return ostr;
    }
  };

  struct Node_hash{
    std::size_t operator () (const Node& n) const
      {
        return (10000*n.x + n.y);
      }
  };

  int count{0};
  int steps{0};
  bool start_received{false};

  bool clear_path_display;

  PixmapBuffer m_buf;
  cv::Mat m_current_path;
  cv::Mat m_planned_path;
  cv::Mat m_depth;
  cv::Mat m_clouds;
  cv::Mat m_water;
  cv::Mat m_vegetation;
  cv::Mat m_temperature;

  cv::Point start;
  cv::Point finish;

  // int t_progress{0};
  int t_clear_path{0};
  int current_x{-1};
  int current_y{-1};

  cv::Point point1;

  std::unordered_set<Node, Node_hash> closed_list;
  std::vector<Node> v_path;
  // std::ofstream outfile;

  // Node node;
  /* create two pixmap buffers,
   * one will have opacity channel, the other
   * don't. */

public:
  explicit PathLayer(Application * app);
  // friend bool operator< (const Node & n1, const Node & n2);
  const cv::Mat materialProperty(PropertyType t);
  
  void render();
  void prepareRender(const QRectF & region);

  void update(PropertyTypes t, QRect rect);
signals:

public slots:
  void receiveCoords(cv::Point received_point);
  void pathReset();

protected:
  void timerEvent(QTimerEvent * event);
  void updateDepth(QRect region);

  void findPath();
  float getCosts(Node actual_node, Node next_node);
  // float getDistance(Node actual_node, Node next_node);
  // float getPriority(Node actual_node, Node next_node);

};



#endif // PATHLAYER_H
