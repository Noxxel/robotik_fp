#ifndef TEMPERATURELAYER_H
#define TEMPERATURELAYER_H


#include "Layer/Layer.h"
#include "Application/PixmapBuffer.h"

class TemperatureLayer : public Layer
{
  Q_OBJECT

  /* create two pixmap buffers,
   * one will have opacity channel, the other
   * don't. */
  // PixmapBuffer m_buf;
  // cv::Mat m_depth;
  cv::Mat m_temperature;
  cv::Mat m_fert;

public:
  explicit TemperatureLayer(Application * app);
  
  const cv::Mat materialProperty(PropertyType t);

  void render();
  void prepareRender(const QRectF & region);

  void update(PropertyTypes t, QRect rect);
  void updateDepth(QRect region);
signals:

public slots:

protected:
  void timerEvent(QTimerEvent * event);

};

#endif // TEMPERATURELAYER_H
