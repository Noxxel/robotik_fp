#ifndef DISPLAYLAYER_H
#define DISPLAYLAYER_H


#include "Layer/Layer.h"
#include "Application/PixmapBuffer.h"

class DisplayLayer : public Layer
{
  Q_OBJECT

  /* create two pixmap buffers,
   * one will have opacity channel, the other
   * don't. */
  PixmapBuffer m_buf;

  PixmapBuffer m_buf2;
  qreal m_buf2Progress;
  QPointF m_buf2Pos;
  cv::Mat m_depth;

public:
  explicit DisplayLayer(Application * app);
  void render();
  void prepareRender(const QRectF & region);

  void update(PropertyTypes t, QRect rect);
  void updateDepth(QRect region);
signals:

public slots:

protected:
  void timerEvent(QTimerEvent * event);

};

#endif // DISPLAYLAYER_H
