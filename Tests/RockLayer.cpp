#include <QtCore/QDebug>

#include "../Application/PixmapBuffer.h"

#include "RockLayer.h"

RockLayer::RockLayer(Application *app)
  : Layer(app),
    m_buf(this, PixmapBuffer::RGBA)
{


    qDebug() << "RockLayer::RockLayer(): " << this->fieldSize();
  /* resize the buffers in order to use them */
  m_buf.setSize(this->fieldSize());


  m_depth = this -> createPropertyBuffer();
  m_fert = this -> createPropertyBuffer();
  m_rock = this -> createPropertyBuffer();
  m_rock.setTo(0);
  
  this->startTimer(100);

}

void RockLayer::render()
{

  /* finally render both buffers */
  m_buf.render();
}

void RockLayer::prepareRender(const QRectF &region)
{
  /* ## here we are going to reduce the physical system to a
   * ## visual representation. Everything that is above 2°C
   * ## is molten water, anything below is ice. */

  /* define scalars that will be used to colorize our pixmap */
  const cv::Scalar black(0.2, 0.2, 0.2, 1);
  const cv::Scalar pink(1.0, 0.0, 1.0, 1);
  const cv::Scalar transp(0.0, 0.0, 0.0, 0);

  /* initially set everything to pink (debug) */
  //m_buf.buffer() = transp;
  m_buf.buffer() = transp;

  /* set the buffer to gray wherever we have stone */
  m_buf.buffer().setTo(black, m_rock == 1);
  m_buf.buffer().setTo(transp, m_depth > 350);

  /* set the render position */
  m_buf.setViewport(QRectF(-1, -1, 2, 2));
}

//is called when something below changes a property (emitpropertchanged())
void RockLayer::update(Layer::PropertyTypes t, QRect rect)
{
  if(t & Fertility){
    m_fert.setTo(0);
    m_fert.setTo(0, m_rock == 1);
  }
}


//returns matrices to layers above
const cv::Mat RockLayer::materialProperty(PropertyType t)
{

  if(t == Layer::Fertility){
    return m_fert;
  }

  /* for anything else we don't care */
  return Layer::materialProperty(t);
}


void RockLayer::timerEvent(QTimerEvent *event)
{
  // m_buf2Progress += 0.01;

  // qreal angle = m_buf2Progress*2.0*M_PI;

  // m_buf2Pos = QPointF(0.25*sin(angle), 0.25*cos(angle));
  // m_buf2Pos -= QPointF(0.25, 0.25);

  /* signal the change */

}

//is called through handledepthinput from application.cpp
void RockLayer::updateDepth(QRect region)
{
  /* set the height */
  m_depth = depth();
  m_rock.setTo(0);
  m_rock.setTo(1, m_depth <= 5);
  m_rock.setTo(1, m_depth >= 80);

  emit materialPropertyChanged(Fertility);
  emit appearanceChanged();
}
