#ifndef DIRTLAYER_H
#define DIRTLAYER_H


#include "Layer/Layer.h"
#include "Application/PixmapBuffer.h"

class DirtLayer : public Layer
{
  Q_OBJECT

  /* create two pixmap buffers,
   * one will have opacity channel, the other
   * don't. */
  PixmapBuffer m_buf;
  cv::Mat m_depth;
  cv::Mat m_fert;
  cv::Mat m_dirt;
  cv::Mat m_dirt_bot;
  cv::Mat m_dirt_top;

public:
  explicit DirtLayer(Application * app);

  const cv::Mat materialProperty(PropertyType t);
  
  void render();
  void prepareRender(const QRectF & region);

  void update(PropertyTypes t, QRect rect);
  void updateDepth(QRect region);
signals:

public slots:

protected:
  void timerEvent(QTimerEvent * event);

};

#endif // DIRTLAYER_H
