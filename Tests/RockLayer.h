#ifndef ROCKLAYER_H
#define ROCKLAYER_H


#include "../Layer/Layer.h"
#include "../Application/PixmapBuffer.h"

class RockLayer : public Layer
{
  Q_OBJECT

  /* create two pixmap buffers,
   * one will have opacity channel, the other
   * don't. */
  PixmapBuffer m_buf;
  cv::Mat m_depth;
  cv::Mat m_fert;
  cv::Mat m_rock;

public:
  explicit RockLayer(Application * app);

  const cv::Mat materialProperty(PropertyType t);
  
  void render();
  void prepareRender(const QRectF & region);

  void update(PropertyTypes t, QRect rect);
  void updateDepth(QRect region);
signals:

public slots:

protected:
  void timerEvent(QTimerEvent * event);

};

#endif // ROCKLAYER_H
