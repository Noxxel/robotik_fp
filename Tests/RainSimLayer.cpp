#include "RainSimLayer.h"

RainSimLayer::RainSimLayer(Application * app)
  : Layer(app)
{
  /* create a buffer that fits to the field size */
  m_depth = this->createPropertyBuffer();

  /* Initialize temperature matrix to 0°C anywhere */
  m_depth = 0.0;
}

const cv::Mat RainSimLayer::materialProperty(PropertyType t)
{

  /* we handle temperature property */
  if(t == Layer::Temperature)
    return m_depth;

  /* for anything else we don't care */
  return Layer::materialProperty(t);
}

void RainSimLayer::update(Layer::PropertyTypes t, QRect rect)
{
  /* We don't need to react on anything here, because
   * either this layer defines the physics (temperature)
   * or we don't care. */
}

void RainSimLayer::timerEvent(QTimerEvent *event)
{
  // m_buf2Progress += 0.01;

  // qreal angle = m_buf2Progress*2.0*M_PI;

  // m_buf2Pos = QPointF(0.25*sin(angle), 0.25*cos(angle));
  // m_buf2Pos -= QPointF(0.25, 0.25);

  /* signal the change */
  // emit appearanceChanged();

}

void RainSimLayer::updateDepth(QRect region)
{

  /* set the temperature according to the height */
  m_depth = (depth());





  emit materialPropertyChanged(Humidity);

}
