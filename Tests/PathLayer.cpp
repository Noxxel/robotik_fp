#include <QtCore/QDebug>

#include "Application/PixmapBuffer.h"

#include "PathLayer.h"

PathLayer::PathLayer(Application *app)
	: Layer(app),
		m_buf(this, PixmapBuffer::RGBA)
{


	qDebug() << "PathLayer::PathLayer(): " << this->fieldSize();
	/* resize the buffers in order to use them */
	m_buf.setSize(this->fieldSize());
	m_depth = this -> createPropertyBufferSquish();
	m_water = this -> createPropertyBufferSquish();
	m_clouds = this -> createPropertyBufferSquish();
	m_vegetation = this -> createPropertyBufferSquish();
	m_temperature = this -> createPropertyBufferSquish();
	m_current_path = this -> createPropertyBuffer();
	m_current_path.setTo(0);
	m_planned_path = this -> createPropertyBuffer();
	m_planned_path.setTo(0);


	// outfile.open("Path.log");

	this->startTimer(150);

	start.x = 220/5;
	start.y = 140/5;

	finish.x = 420/5;
	finish.y = 340/5;

	clear_path_display = false;
}


void PathLayer::render()
{
	m_buf.render();
	/* finally render both buffers */
}

void PathLayer::prepareRender(const QRectF &region)
{

	const cv::Scalar transp(1.0, 1.0, 1.0, 0);
	const cv::Scalar white(1.0, 1.0, 1.0, 1);
	const cv::Scalar whitetransp(1.0, 1.0, 1.0, 0.5);
	const cv::Scalar red(1.0, 0.0, 0.0, 1);


	m_buf.buffer().setTo(transp);
	m_buf.buffer().setTo(white, m_planned_path == 1);
	m_buf.buffer().setTo(whitetransp, m_current_path == 1);
	m_buf.buffer().setTo(red, m_current_path >= 2);

	m_buf.setViewport(QRectF(-1, -1, 2, 2));


}

void PathLayer::update(Layer::PropertyTypes t, QRect rect)
{
	if(t &Water) {
		cv::resize(Layer::materialProperty(Water), m_water, m_water.size());
		m_water.setTo(0, m_water < 1);
		m_water.setTo(1, m_water > 1);
		cv::blur(m_water, m_water, cv::Size(3, 3));
		m_water.setTo(1, m_water > 0.1);
	}
	if(t &Temperature) {
		cv::resize(Layer::materialProperty(Temperature), m_temperature, m_temperature.size());
	}
	if(t &Vegetation) {
		cv::resize(Layer::materialProperty(Vegetation), m_vegetation, m_vegetation.size());
	}
	if(t &RenderedClouds) {
		cv::resize(Layer::materialProperty(RenderedClouds), m_clouds, m_clouds.size());
	}




	/* check if temperature has changed */

		// emit appearanceChanged(rect);
}

const cv::Mat PathLayer::materialProperty(PropertyType t)
{

	/* for anything else we don't care */
	return Layer::materialProperty(t);
}

//add depthFunction 


void PathLayer::timerEvent(QTimerEvent *event){
	// if(t_progress < 250){
	// 	t_progress++;
	// }
	// if(t_progress == 220){
	// 	findPath();
	// }

	if(v_path.size() > 0){
		if(v_path.size() >1){
			if(current_x < v_path.at(1).x*5){
				current_x++;
			}else if(current_x > v_path.at(1).x*5){
				current_x--;
			}

			if(current_y < v_path.at(1).y*5){
				current_y++;
			}else if(current_y > v_path.at(1).y*5){
				current_y--;
			}

			m_current_path.setTo(1, m_current_path > 1);


			m_current_path.at<float>(current_y, current_x) = 2;

			m_current_path.at<float>(current_y-1, current_x-1) = 2;
			m_current_path.at<float>(current_y-1, current_x) = 2;
			m_current_path.at<float>(current_y-1, current_x+1) = 2;
			m_current_path.at<float>(current_y, current_x+1) = 2;
			m_current_path.at<float>(current_y+1, current_x+1) = 2;
			m_current_path.at<float>(current_y+1, current_x) = 2;
			m_current_path.at<float>(current_y+1, current_x-1) = 2;
			m_current_path.at<float>(current_y, current_x-1) = 2;


			if(steps == 5){
				v_path.erase(v_path.begin());
				start.x = current_x/5;
				start.y = current_y/5;
				steps = 0;
				findPath();
			}
		}else{
			if(current_x < v_path.at(0).x*5){
				current_x++;
			}else if(current_x > v_path.at(0).x*5){
				current_x--;
			}

			if(current_y < v_path.at(0).y*5){
				current_y++;
			}else if(current_y > v_path.at(0).y*5){
				current_y--;
			}

			m_current_path.setTo(1, m_current_path > 1);


			m_current_path.at<float>(current_y, current_x) = 2;

			m_current_path.at<float>(current_y-1, current_x-1) = 2;
			m_current_path.at<float>(current_y-1, current_x) = 2;
			m_current_path.at<float>(current_y-1, current_x+1) = 2;
			m_current_path.at<float>(current_y, current_x+1) = 2;
			m_current_path.at<float>(current_y+1, current_x+1) = 2;
			m_current_path.at<float>(current_y+1, current_x) = 2;
			m_current_path.at<float>(current_y+1, current_x-1) = 2;
			m_current_path.at<float>(current_y, current_x-1) = 2;


			if(steps == 5){
				v_path.erase(v_path.begin());
				start.x = current_x/5;
				start.y = current_y/5;
				steps = 0;
				if(v_path.size() < 1){
					clear_path_display = true;
				}
			}
		}
		steps++;
		emit appearanceChanged();
	}

	if(clear_path_display && t_clear_path < 20){
		t_clear_path++;
	}else if(clear_path_display && t_clear_path == 20){
		m_current_path.setTo(0);
		m_planned_path.setTo(0);
		emit appearanceChanged();
		t_clear_path = 0;
		clear_path_display = false;
	}
}

void PathLayer::updateDepth(QRect region)
{

	/* set the height */
	cv::resize(depth(), m_depth, m_depth.size());

}

void PathLayer::findPath(){
	Node first_node;
	first_node.x = start.x;
	first_node.y = start.y;
	first_node.cost = 0;
	first_node.distance = abs(first_node.x - finish.x) + abs(first_node.y - finish.y);
	first_node.priority = 0;
	first_node.parent_y = -1;
	first_node.parent_x = -1;
	v_path.clear();
  std::priority_queue<Node> open_list;
	closed_list.clear();
	open_list.push(first_node);
	// outfile<<"datas of start_node: "<<first_node<<std::endl;

	count = 0;
	bool dest_found = false;
	while(!dest_found && count < 15000){

		// outfile<<"_______________________"<<std::endl;
		// outfile<<"number of runs: "<<count<<std::endl;
		Node current_node;

		// pull current best node
		if(open_list.size() <= 0){
			std::cout<<"priority list broken"<<std::endl;
			break;
		}

		current_node = open_list.top();
		open_list.pop();

		if(closed_list.count(current_node) < 1){
			if(current_node.distance == 0){
				v_path.push_back(current_node);
				dest_found = true;
				break;
			}

			closed_list.emplace(current_node);

			// outfile<<"datas of current_node: "<<current_node<<std::endl;

			// add neighbours to open_list
			Node upper_neighbour;
			upper_neighbour.x = current_node.x;
			upper_neighbour.y = current_node.y-1;
			if(closed_list.count(upper_neighbour) < 1 && upper_neighbour.x > 0 && upper_neighbour.x < 640/5 && upper_neighbour.y > 0 && upper_neighbour.y < 480/5){
				upper_neighbour.parent_x = current_node.x;
				upper_neighbour.parent_y = current_node.y;
				upper_neighbour.cost = getCosts(current_node, upper_neighbour);
				upper_neighbour.distance = abs(finish.x - upper_neighbour.x) + abs(finish.y - upper_neighbour.y);
				upper_neighbour.priority = upper_neighbour.cost + upper_neighbour.distance;
				open_list.push(upper_neighbour);
				// outfile<<"datas of upper_neighbour: "<<upper_neighbour<<std::endl;
			}
			
			Node left_neighbour;
			left_neighbour.x = current_node.x-1;
			left_neighbour.y = current_node.y;
			if(closed_list.count(left_neighbour) < 1 && left_neighbour.x > 0 && left_neighbour.x < 640/5 && left_neighbour.y > 0 && left_neighbour.y < 480/5){
				left_neighbour.parent_x = current_node.x;
				left_neighbour.parent_y = current_node.y;
				left_neighbour.cost = getCosts(current_node, left_neighbour);
				left_neighbour.distance = abs(finish.x - left_neighbour.x) + abs(finish.y - left_neighbour.y);
				left_neighbour.priority = left_neighbour.cost + left_neighbour.distance;
				open_list.push(left_neighbour);
				// outfile<<"datas of left_neighbour: "<<left_neighbour<<std::endl;
			}

			Node right_neighbour;
			right_neighbour.x = current_node.x+1;
			right_neighbour.y = current_node.y;
			if(closed_list.count(right_neighbour) < 1 && right_neighbour.x > 0 && right_neighbour.x < 640/5 && right_neighbour.y > 0 && right_neighbour.y < 480/5){
				right_neighbour.parent_x = current_node.x;
				right_neighbour.parent_y = current_node.y;
				right_neighbour.cost = getCosts(current_node, right_neighbour);
				right_neighbour.distance = abs(finish.x - right_neighbour.x) + abs(finish.y - right_neighbour.y);
				right_neighbour.priority = right_neighbour.cost + right_neighbour.distance;
				open_list.push(right_neighbour);
				// outfile<<"datas of right_neighbour: "<<right_neighbour<<std::endl;	
			}

			Node bottom_neighbour;
			bottom_neighbour.x = current_node.x;
			bottom_neighbour.y = current_node.y+1;
			if(closed_list.count(bottom_neighbour) < 1 && bottom_neighbour.x > 0 && bottom_neighbour.x < 640/5 && bottom_neighbour.y > 0 && bottom_neighbour.y < 480/5){
				bottom_neighbour.parent_x = current_node.x;
				bottom_neighbour.parent_y = current_node.y;
				bottom_neighbour.cost = getCosts(current_node, bottom_neighbour);
				bottom_neighbour.distance = abs(finish.x - bottom_neighbour.x) + abs(finish.y - bottom_neighbour.y);
				bottom_neighbour.priority = bottom_neighbour.cost + bottom_neighbour.distance;
				open_list.push(bottom_neighbour);
				// outfile<<"datas of bottom_neighbour: "<<bottom_neighbour<<std::endl;
			}
			//all neighbours added to priority list.


			// std::cout<<current_node<<std::endl;
			// std::cout<<"Size: "<<v_path.size()<<std::endl;

			count++;

			// outfile<<"size of the open_list: "<<open_list.size()<<std::endl;
		}
	}
	if(count >= 15000){
		std::cout<<"_________________________________________SHITS FUCKED YOOOOO ENDLOS LOOP"<<std::endl;
	}


	if(v_path.size() > 0){
		while(v_path.at(0).parent_x != -1){
			Node find_parent;
			find_parent.y = v_path.at(0).parent_y;
			find_parent.x = v_path.at(0).parent_x;
			v_path.emplace(v_path.begin(), *(closed_list.find(find_parent)));
		}
		m_planned_path.setTo(0);
		for(int i = 0; i < v_path.size(); i++){
			m_planned_path.at<float>(v_path.at(i).y*5, v_path.at(i).x*5) = 1;
		}
		current_x = v_path.at(0).x*5;
		current_y = v_path.at(0).y*5;
	}

	// std::cout<<v_path.size()<<std::endl;
}

float PathLayer::getCosts(Node actual_node, Node next_node){
	if(m_depth.at<float>(next_node.y, next_node.x) > 300){
		return 500000;
	}



	float temp_cost = 1;

	//clouds
	if(m_clouds.at<float>(next_node.y, next_node.x) >= 0.2){
		temp_cost += m_clouds.at<float>(next_node.y, next_node.x) * 100;
	}

	//depth down
	if(m_depth.at<float>(actual_node.y, actual_node.x) >= m_depth.at<float>(next_node.y, next_node.x)){
		temp_cost += (m_depth.at<float>(actual_node.y, actual_node.x) - m_depth.at<float>(next_node.y, next_node.x)) * 0.05;
	}
	//depth up
	else if(m_depth.at<float>(actual_node.y, actual_node.x) < m_depth.at<float>(next_node.y, next_node.x)){
		temp_cost += (m_depth.at<float>(next_node.y, next_node.x) - m_depth.at<float>(actual_node.y, actual_node.x)) * 1.75;
	}

	//water
	if(m_water.at<float>(next_node.y, next_node.x) >= 1){
		temp_cost = 125;
	}

	//temperature
	if(m_temperature.at<float>(next_node.y, next_node.x) < 15){
		temp_cost += (15 - m_temperature.at<float>(next_node.y, next_node.x)) * 0.2;
	}else if(m_temperature.at<float>(next_node.y, next_node.x) > 25){
		temp_cost += (m_temperature.at<float>(next_node.y, next_node.x) - 25) * 0.2;
	}

	//vegetation
	if(m_vegetation.at<float>(next_node.y, next_node.x) > 0.5){
		temp_cost += m_vegetation.at<float>(next_node.y, next_node.x) * 4;
	}else if(m_vegetation.at<float>(next_node.y, next_node.x) < 0.1){
		temp_cost += 1;
	}

	return temp_cost + actual_node.cost;

}

// float PathLayer::getDistance(Node actual_node, cv::Point finish){
// 	float priority = abs(getXPos(actual_node) - finish.x) + abs(getYPos(actual_node) - finish.y);
// 	return priority;
// }

// float PathLayer::getPriority(Node actual_node, Node next_node){
// 	// return get
// }

// int PathLayer::getXPos(Node actual_node){
// 	return x;
// }

// int PathLayer::getYPos(Node actual_node){
// 	return y;
// }


void PathLayer::receiveCoords(cv::Point received_point){
	if(m_depth.at<float>(received_point.y/5, received_point.x/5) <= 150){
		if(start_received){
			std::cout<<"end point set!"<<std::endl;
			finish.y = received_point.y/5;
			finish.x = received_point.x/5;
			findPath();
		}else{
			std::cout<<"start point set!"<<std::endl;
			start.y = received_point.y/5;
			start.x = received_point.x/5;
			start_received = true;
		}
	}
}

void PathLayer::pathReset(){
	start_received = false;
	steps = 0;
	v_path.clear();
	clear_path_display = true;
}