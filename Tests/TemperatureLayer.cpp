#include <QtCore/QDebug>

#include "Application/PixmapBuffer.h"

#include "TemperatureLayer.h"

TemperatureLayer::TemperatureLayer(Application *app)
  : Layer(app)
    // m_buf(this, PixmapBuffer::RGBA)
{


  qDebug() << "TemperatureLayer::TemperatureLayer(): " << this->fieldSize();

  /* resize the buffers in order to use them */
  // m_buf.setSize(this->fieldSize());

  // m_depth = this -> createPropertyBuffer();
  m_temperature = this -> createPropertyBuffer();
  m_fert = this -> createPropertyBuffer();


  this->startTimer(100);

}

void TemperatureLayer::render()
{

  /* finally render both buffers */
  //m_buf.render();
}

void TemperatureLayer::prepareRender(const QRectF &region)
{


  /* ## here we are going to reduce the physical system to a
   * ## visual representation. Everything that is above 2°C
   * ## is molten water, anything below is ice. */

  /* define scalars that will be used to colorize our pixmap */
  // cv::Scalar transp(0.0, 0.0, 0.0, 0);
  // cv::Scalar lightblue(0.0, 0.3, 1.0, 0.5);
  // cv::Scalar tempBlack(0.0, 0.0, 0.0, 1);

  // /* set the buffer to lightblue below 20 */
  // m_buf.buffer() = transp;
  // m_buf.buffer().setTo(tempBlack, m_depth<=10.0);
  // m_buf.buffer().setTo(tempBlack, m_temperature<=35);
  // m_buf.buffer().setTo(lightblue, m_buf.buffer() == tempBlack);


  // /* set the render position */
  // m_buf.setViewport(QRectF(-1, -1, 2, 2));

}

void TemperatureLayer::update(Layer::PropertyTypes t, QRect rect)
{

}


const cv::Mat TemperatureLayer::materialProperty(PropertyType t)
{
  /* we handle temperature property */
  if(t == Layer::Temperature){
    return m_temperature;
  }

  
  /* for anything else we don't care */
  return Layer::materialProperty(t);
}

//add depthFunction 


void TemperatureLayer::timerEvent(QTimerEvent *event)
{

  // m_buf2Progress += 0.01;

  // qreal angle = m_buf2Progress*2.0*M_PI;

  // m_buf2Pos = QPointF(0.25*sin(angle), 0.25*cos(angle));
  // m_buf2Pos -= QPointF(0.25, 0.25);

  /* signal the change */
  emit appearanceChanged();



}

void TemperatureLayer::updateDepth(QRect region)
{

  /* set the height */
  //m_depth = depth();

  m_temperature = 42+(-0.50*depth());

  emit materialPropertyChanged(Temperature);
}
