project(SandboxFramework)
cmake_minimum_required(VERSION 2.8)

# enable c++11 features for project
if(UNIX)
    SET(CMAKE_C_COMPILER "/usr/bin/clang")
    SET(CMAKE_CXX_COMPILER "/usr/bin/clang++")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -std=c++11")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fcolor-diagnostics") # for ninja
endif()

###
# This macro takes a list of sources and prepends the relative path of all files.
# finally it exports the list to a global scope variable with name GLOBAL_VAR.
# this is handy for assembling from subdirectories
macro (add_files GLOBAL_VAR)
    file (RELATIVE_PATH _relPath "${${PROJECT_NAME}_SOURCE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}")
    foreach (_src ${ARGN})
        if (_relPath)
            list (APPEND SRC "${_relPath}/${_src}")
        else()
            list (APPEND SRC "${_src}")
        endif()
    endforeach()
    if (_relPath)
        # propagate SRCS to parent directory
        set (${GLOBAL_VAR} ${${GLOBAL_VAR}} ${SRC} PARENT_SCOPE)
    endif()
    set(LOCAL_LIST GLOBAL_VAR)
    message(STATUS "${GLOBAL_VAR}: adding ${ARGN}")
endmacro()

set(CMAKE_AUTOMOC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Qt5Core REQUIRED)
find_package(Qt5Gui REQUIRED)

find_package(OpenCV REQUIRED)

include_directories(.)


option(WITH_KINECT "enables kinect device driver" OFF)



if(WITH_KINECT)
    add_definitions(-DWITH_KINECTDEVICE)
    message("Building with libfreenect")
    set(libfreenect_libs "freenect")
endif()


# add different subdirectories
add_subdirectory(Application)
add_subdirectory(Input)
add_subdirectory(Layer)

# Test executables
add_subdirectory(Tests)

# create library and link to required libraries
add_library(SandboxCore STATIC ${files_LIB_SANDBOX})
qt5_use_modules(SandboxCore Core Gui)
target_link_libraries(SandboxCore
                      ${OpenCV_LIBS}
                      ${QT_QTGUI_LIBRARY}
                      GL
                      ${libfreenect_libs})



