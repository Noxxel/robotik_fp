#ifndef LAYER_H
#define LAYER_H

#include <QtCore/QRect>
#include <QtCore/QObject>
#include <QtCore/QMap>
#include <QtGui/QOpenGLFunctions>
#include <QtGui/QOpenGLPaintDevice>
#include <QOpenGLBuffer>
#include <iostream>
#include <vector>
#include <queue>
#include <set>


#include <opencv2/core/core.hpp>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui_c.h>

#include "Application/PixmapBuffer.h"

class Application;

/**
 * @brief The Layer class
 *
 * A layer represents a functional layer on the graphic stack.
 * It offers a possibility to render with openGL using render()
 * and has Layer::materialProperties() to expose or change material properties
 * of the given layer. These material properties are used for simulation purposes.
 *
 * Layers are stacked such that
 * the upmost layer takes precedence over all information (meta and visual).
 * Every layer can partially hide or change information given from the layer below,
 * expose completetly new information or leave it unchanged.
 *
 * This gives rise to fine-grained layer functionality.
 * One kind of layers does numerical (enviromental) computation
 * while other are responsible for displaying this processes. This makes them quite
 * exchangeable.
 *
 * Most important functions are updateMaterialProperties() and render().
 *
 * The first slot is called whenever any dependency to lower Layers needs to trigger
 * an update, while the second slot is responsible for rendering the layer.
 *
 * Both functions need to be reimplemented in derived Layers in order to expose
 * a special functionality.
 * 
 *
 * The visual coordinate system of a layer is defined by the rectangle spanned
 * by the points (-1,1) and (1,1), where the precedent point is the lower-left and
 * the subsequent one is the upper-right corner.
 *
 *
 *
 */
class Layer : public QObject, protected QOpenGLFunctions
{
public:

  enum PropertyType {
    RenderedClouds = 1, /*!< unitless quantity describing how fast a mass will sink into ground */
    Temperature = 2 , /*!< Temperature in the field [°C] */
    Fertility = 4 ,
    Humidity = 8,
    Water = 16, /*!< fractional number in [0,1], where 0 is dry ground and 1 corresponds to pure water */
    Vegetation = 32,
    Clouds = 64,

    //These two need to be set! eMin is the first Property (Value:0), eMax the last
    ePropertyTypeMin=RenderedClouds,
    ePropertyTypeMax=Clouds

  };

  Q_DECLARE_FLAGS(PropertyTypes, PropertyType)

private:
  /* give Application instance access to private members */
  friend class Application;

  Q_OBJECT

  /* registers enum to qt's type system */
  Q_ENUMS(PropertyType)


private:


  Application * m_app;

  QList<PixmapBuffer*> m_pixmapBuffers;

  /** Stores pointer to previous layer on stack. */
  Layer* m_prevLayer;

  bool m_visualChanged;
  QRectF m_visualChangedRegion;

  /** Accumulated changes since last call to update(). */
  PropertyTypes m_changed;

  /** Accumulated changed region since last update(). */
  QRect m_changedRegion;

private:

  /**
   * Stores any resulting changes that were signalled via
   * materialPropertiesUpdated().
   */
  void storePropertyChanges(PropertyTypes t, QRect region);

  void storeVisualChanged(QRectF region);

  /** this function binds the openGL buffer to PixmapBuffers
   * right before prepareRender() call.
   */
  void bindPixmapBuffers();

  /**
   * unbinds the openGL buffer again.
   * @see bindPixmapBuffers()
   */
  void unbindPixmapBuffers();

protected:

   /** intercept child add event to handle Pixmapbuffer Objects */
   void childEvent(QChildEvent * event);

   /**
    * @returns a matrix with same dimensions as required for material properties.
    * This is just a helper function.
    */
   cv::Mat createPropertyBuffer() const;
   cv::Mat createPropertyBufferSquish() const;
   // cv::Mat createPropertyBufferInt() const;

   /**
    * @brief blendMatrices linearly blends two matrices with given blend factor
    * @param A
    * @param B
    * @param result the resulting matrix
    * @param blendFactor 0...1
    * @return A*(blendFactor) + B*(1-blendFactor)
    *
    * This is a helper function that can be used to build a mixed
    * version of material properties.
    *
    * Both matrices must have the same size and type.
    */
   static void blendMatricesLinear(const cv::Mat & A, const cv::Mat & B,
                                   cv::Mat & result, double blendFactor);


   /**
    * @return a rect with relative coordinates to physical viewport.
    * Relative coordinates follows openGL convention where the full viewport
    * is spanned by [-1, 1]x[-1,1].
    */
   QRectF physicalToRelative(const QRect & rect) const;

   /**
    * @return a rect with physical coordinates given by a relative rect
    * @see physicalToRelative() for further information
    */
   QRect relativeToPhysical(const QRectF & rect) const;

   /**
    * @return the size of the simulation field
    * this size is defined by the input sensor resolution.
    */
   QSize fieldSize() const;

   /**
    * returns a reference to a depth map
    *
    * The size of the depth map will be Layer::fieldSize()
    * The unit of the depthmap is in meters.
    *
    * See Application::setHeightScaling() to globaly adjust the
    * terrain mapping.
    *
    * @param step -3...0, the time step you want to get depth map for
    * @returns a Matrix with type CV_32F (float)
    *
    */
   const cv::Mat depth(int step = 0);

   /**
    * returns a reference to a camera image
    * @param step -3...0, the time step you want to get camera image for
    *
    * @returns a matrix of type CV_8UC3 (color image, 1byte per channel)
    */
   const cv::Mat camera(int step = 0);

public:
  explicit Layer(Application *app);
  
  cv::Rect_<int> QTtoCVrect(QRect);

  /** returns the previous layer in Application's stack */
  Layer * previousLayer();


  /** returns the layer mask. Size is equal to fieldSize(). */
  virtual cv::Mat mask() const;
  
  /**
   * @brief computes a compounding rectangle including all different pixel
   *        between A and B.
   * @param A one of the matrix
   * @param B other matrix
   */
  static QRect differenceRegion(const cv::Mat & A,
                                const cv::Mat & B);

  /**
   * Reimplement this function if you need to do some initializing
   * that can't be done due to unavailable information withing the constructor.
   * One example is setting up openGL structures, like PixmapBuffer.
   *
   * This function hence is guaranteed to be called from within
   * the gui thread/context.
   *
   */
  virtual void initialize() {}

  /**
   * This function gives access to different layer properties
   * like viscosity or temperature.
   *
   * It is a 2D float valued Matrix with one channel (CV_32F).
   *
   * The default implementation passes the information from the underlaying
   * layer through.
   *
   * Reimplement this function if you want to modify the information or
   * if you somehow need to blend the values.
   *
   * @param t property that should be retrieved
   * @return matrix for given MaterialType
   *
   * @warning Don't work on shallow copies of the returned instance.
   *          Always make deep copies, befor modifying content!
   *
   * @see update()
   *
   */
  virtual const cv::Mat materialProperty(PropertyType  t);

signals:

  /**
   * Signalizes a change in the given material properties.
   * Provide a QRect to indicate the area that has been changed.
   *
   * If the region is null, the whole field is considered to be changed.
   *
   */
  void materialPropertyChanged(PropertyTypes t, QRect region = QRect());

  /**
   * Signal is emitted whenever something to the visual appearance has changed.
   * This will trigger a render update in Application instance.
   * You can specify a subregion, to signal that only a partial
   * update to the diplay buffers is needed.
   *
   * If region is null, the whole viewport is considered to be changed.
   */
  void appearanceChanged(QRectF region = QRectF());


public slots:


  /**
   * This function is called when the layer should be rendered
   * by the openGL engine. Caller is the GUI thread with
   * active openGL context.
   *
   * You can use any openGL command you like.
   *
   * Reimplement this in order to draw something.
   *
   */
  virtual void render() {}

  /**
   * @brief prepareRender
   * @param region subregion that should be updated.
   *
   * This function will be called when something in the visual appearance
   * has changed. Caller is the GUI thread. However, during execution
   * of this function, any calls to update() will be blocked.
   *
   * You should use this function as synchronized interface between simulation
   * and GUI threads.
   *
   * More abstractly spoken, use this function to reduce information from the
   * physical system to a graphical representation.
   *
   * Reimplement this function to update pixmaps etc. that should be rendered.
   *
   */
  virtual void prepareRender(const QRectF & region) {}

  /**
   *
   * @param t PropertyTypes that have changed below this layer
   * @param rect region where changes happened
   *
   * This pure virtual function has to be reimplemented in order to
   * provide a layer functionality on the simulation side.
   *
   * Put any heavy computation inside this scope.
   *
   */
  virtual void update(PropertyTypes t, QRect rect = QRect()) = 0;

  /**
   * Reimplement this function to react on changes to the hightmap.
   * You can use Layer::depth() to retrieve the current map.
   */
  virtual void updateDepth(QRect region) {}

  virtual void updateDepthHigh(QRect region) {}

  /**
   * Reimplement this function to react on changes to the depth map
   * You can use Layer::camera() to retrieve the current camera image.
   */
  virtual void updateCamera(QRect region) {}
};

Q_DECLARE_OPERATORS_FOR_FLAGS(Layer::PropertyTypes)

#endif // LAYER_H
