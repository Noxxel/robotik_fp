#include <QtCore/QDebug>

#include "Layer/Layer.h"
#include "Application/Application.h"

#include<iostream>

Layer::Layer(Application *app) :
  QObject(app),
  m_app(app),
  m_prevLayer(0),
  m_changed(0)
{

  /* Needs to be called in order to resolve openGL calls
   * that are provided by QOpenGLFunctions. */
  this->initializeOpenGLFunctions();

  /* Connect the changed signal */
  connect(this, &Layer::materialPropertyChanged,
          this, &Layer::storePropertyChanges);

  connect(this, &Layer::appearanceChanged,
          this, &Layer::storeVisualChanged);
}

void Layer::storePropertyChanges(PropertyTypes t, QRect region)
{
//  qDebug() << this << "storePropertyChanges(): " << t << region;
  m_changed |= t;
  m_changedRegion |= region;
}

void Layer::storeVisualChanged(QRectF region)
{
  m_visualChanged = true;
  m_visualChangedRegion = region;
}

void Layer::bindPixmapBuffers()
{
  /** @todo implement me */
}

void Layer::unbindPixmapBuffers()
{
  /** @todo implement me */
}

void Layer::childEvent(QChildEvent *event)
{

  /* intercept ChildAdded event and catch any PixmapBuffers */
  if(event->type() == QEvent::ChildAdded) {

      PixmapBuffer * b = qobject_cast<PixmapBuffer*>(event->child());
      if(b) {
          m_pixmapBuffers.append(b);
         }
    }

  /* pass event down to default implementation */
  QObject::childEvent(event);

}

cv::Mat Layer::createPropertyBuffer() const
{
  QSize size = this->fieldSize();
  return cv::Mat(size.height(), size.width(),
                 CV_32F, 0.0);
}

cv::Mat Layer::createPropertyBufferSquish() const
{
  QSize size = this->fieldSize();
  return cv::Mat(size.height()/5, size.width()/5,
                 CV_32F, 0.0);
}

// cv::Mat Layer::createPropertyBufferInt() const
// {
//   QSize size = this->fieldSize();
//   return cv::Mat(size.height(), size.width(),
//                  CV_8S, 0.0);
// }


/* takes a QRect and converts it to cv::rect.....I don't know yet what happens if you pass a NUll Qrect
 * ....better exclude that case before running this function */
cv::Rect_<int> Layer::QTtoCVrect(QRect rect)
{
	QPoint p=rect.topLeft();
	cv::Rect_<int> new_rect(p.x(),p.y(),rect.width(),rect.height());
	return new_rect;
}


Layer* Layer::previousLayer()
{
        return m_prevLayer;
}

cv::Mat Layer::mask() const
{
  return cv::Mat();
}

QRect Layer::differenceRegion(const cv::Mat &A,
                              const cv::Mat &B)
{
  //	cv::Mat data=mergeUpdate(t);
  //	QPoint p;
  //	QPoint q;
  //	int p_x=-1;
  //	int p_y=-1;
  //	int q_x=-1;
  //	int q_y=-1;
  //	bool found=false;
  //	bool edge=false;
  //	//find upper left und lower right corner of future rect
  //	for(int i=0;i<data.size().height;i++)
  //	{
  //		if(!edge)
  //		{
  //		for(int j=0;j<data.size().width;j++)
  //		{
  //			if(found)
  //			{
  //				if(j>=p_x)
  //				{
  //					j=data.size().width;
  //					break;
  //				}
  //				else
  //				{
  //					if(data.at<int>(i,j) != 0)
  //					{
  //						p_x=j;
  //						if(j==0) edge=true;
  //					}
  //				}
  //			}
  //			else
  //			{
  //				if(data.at<int>(i,j) != 0)
  //				{
  //					p_y=i;
  //					p_x=j;
  //					found=true;
  //					break;
  //				}
  //			}
  //		}
  //		}
  //	}
  //	p=QPoint(p_x,p_y);
  //	found=false;
  //	edge=false;
  //	//bottom right
  //	for(int i=data.size().height-1;i>=0;i--)
  //	{
  //		if(!edge)
  //		{
  //		for(int j=data.size().width-1;j>=0;j--)
  //		{
  //			if(found)
  //			{
  //				if(j<=q_x)
  //				{
  //					j=-1;
  //					break;
  //				}
  //				else
  //				{
  //					if(data.at<int>(i,j) != 0)
  //					{
  //						q_x=j;
  //						if(j==data.size().width-1) edge=true;
  //					}
  //				}
  //			}
  //			else
  //			{
  //				if(data.at<int>(i,j) != 0)
  //				{
  //					q_y=i;
  //					q_x=j;
  //					found=true;
  //					break;
  //				}
  //			}
  //		}
  //		}
  //	}
  //	q=QPoint(q_x,q_y);
  //	if(q_x == -1 || p_x == -1)
  //		return QRect();
  //	else
  //	{
  //		return QRect(p,q);
  //	}

  return QRect();
}



void Layer::blendMatricesLinear(const cv::Mat &A, const cv::Mat &B,
                                   cv::Mat & result, double blendFactor)
{
  /* check that the outer dimensions etc. match  */
  Q_ASSERT(blendFactor >= 0.0 && blendFactor <= 1.0);
  Q_ASSERT(A.size == B.size);
  Q_ASSERT(A.type() == B.type());

  result =  A*blendFactor+B*(1.0-blendFactor);
}

QRectF Layer::physicalToRelative(const QRect &rect) const
{
  /** @todo implement me */
  return QRectF();
}

QRect Layer::relativeToPhysical(const QRectF &rect) const
{
  /** @todo implement me */
  return QRect();
}

const cv::Mat Layer::materialProperty(PropertyType t)
{
  Q_ASSERT(m_prevLayer);
  return m_prevLayer->materialProperty(t);
}

QSize Layer::fieldSize() const
{
  return m_app->m_input->depthSize();
}

const cv::Mat Layer::depth(int step)
{
  Q_ASSERT(step <= 0 && step > -4);
  return m_app->m_depthInput;
}

const cv::Mat Layer::camera(int step)
{
  Q_ASSERT(step <= 0 && step > -4);
  return m_app->m_input->cam();
}

