#include <QtCore/QDebug>
#include <QtGui/QWindow>
#include <QtGui/QResizeEvent>

#include <QtCore/QCoreApplication>

#include "OpenGLWindow.h"
#include "Application.h"

void OpenGLWindow::resizeEvent(QResizeEvent *event)
{

  glLoadIdentity();


}

OpenGLWindow::OpenGLWindow(Application * app)
    : m_app(app),
      m_update_pending(false),
      m_context(0),
      m_needsInitialize(true)

{
    setSurfaceType(QWindow::OpenGLSurface);
    this->create();

    /* Create a openGL context */
    m_context = new QOpenGLContext(this);
    m_context->setFormat(requestedFormat());
    m_context->create();
    m_context->makeCurrent(this);

    /* Needs to be called in order to resolve openGL calls
     * that are provided by QOpenGLFunctions. */
    this->initializeOpenGLFunctions();

}

OpenGLWindow::~OpenGLWindow()
{

}

void OpenGLWindow::initialize()
{
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);


  // //adjust for misplaced beamer/kinect
  // glScalef(1.17,1.25,1);
  // glTranslatef(0,-0.215,0);


  //adjust for misplaced beamer/kinect
  glScalef(1.22,1.235,1);
  glTranslatef(0.0,-0.23,0);

  // GLdouble xmin, xmax, ymin, ymax;

  // ymax = 1 * tan( 45 * M_PI / 360.0 );
  // ymin = -ymax;
  // xmin = ymin * 4/3;
  // xmax = ymax * 4/3;

  // glFrustum( xmin, xmax, ymin, ymax, 1, 500 );

  // glRotatef(30, 1, 0, 0);
  
}

void OpenGLWindow::render()
{
  glClear(GL_COLOR_BUFFER_BIT
          | GL_DEPTH_BUFFER_BIT
          | GL_STENCIL_BUFFER_BIT);
  /* set viewport to window size */
  glViewport(0.0, 0.0, this->width(), this->height());


  /* and finally render all the layers */
  if(m_app)
    m_app->renderLayers();

}

void OpenGLWindow::renderLater()
{
    if (!m_update_pending) {
        m_update_pending = true;
        QCoreApplication::postEvent(this, new QEvent(QEvent::UpdateRequest));
    }
}

void OpenGLWindow::renderNow()
{
    if (!isExposed())
        return;

    /* make our context the current openGL context */
    m_context->makeCurrent(this);

    /* check, if we still need to initialize something */
    if (m_needsInitialize) {
        initialize();
        m_needsInitialize = false;
    }

    /* render the scene content */
    render();
    // std::cout<<"rendering!"<<std::endl;

    /* double buffering update */
    m_context->swapBuffers(this);
}

bool OpenGLWindow::event(QEvent *event)
{
    switch (event->type()) {
    case QEvent::UpdateRequest:
        m_update_pending = false;
        renderNow();
        return true;
    default:
        return QWindow::event(event);
    }
}

void OpenGLWindow::exposeEvent(QExposeEvent *event)
{
    Q_UNUSED(event);

    if (isExposed())
        renderNow();
}
