#ifndef APPLICATION_H
#define APPLICATION_H

#include <QtCore/QObject>
#include <QtCore/QChildEvent>
#include <QCache>

#include <QtGui/QGuiApplication>
#include <QtGui/QOpenGLFunctions>

#include "OpenGLWindow.h"
#include "Layer/Layer.h"
#include "Input/InputDevice.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui_c.h>

/**
 * This class is the entry point to all functionality given by this framework.
 *
 * You can register Layer that perform simulation and provide possibilities
 * to give visual feedback of the simulation.
 *
 * Have a look into the Tests folder for a quite comprehensive example
 * on the usage of this frameork.
 *
 * Currently there is a TestInput device implemented, that exposes
 * a dynamic hightmap that is similar to a gaussian peak.
 *
 *
 * TODOs on Input System
 * =====================
 *
 * - add file playback device driver
 * - implement denoising of input
 * - implement thresholded updates
 * - implement dectection of changed regions in Input
 *
 * TODOs on Visual System
 * ======================
 * - undistort projection on sandbox
 *
 * TODOs on Processing System
 * ==========================
 * - split up display and simulation into two different threads
 * - implement locks for Layer::prepareRender()
 * - Map PixmapBuffer into GPU memory during Layer::prepareRender() call
 *
 * TODOs in general
 * ================
 * - grep through the code for "@todo" :)
 *
 */

class Application : public QObject, protected QOpenGLFunctions
{

  friend class OpenGLWindow;
  friend class Layer;

  Q_OBJECT

  QGuiApplication m_app;
  OpenGLWindow * m_window;

  InputDevice * m_input;
  cv::Mat m_depthInput;
  cv::Mat average;
  cv::Mat last_depth;
  cv::Mat new_depth;
  cv::Mat m_camInput;
  int depthInputCounter;

  float m_inputOffset;
  float m_inputScaling;


  QList<Layer*> m_layer;

  bool m_propertyUpdateScheduled;

  QTimer m_displayTimer;
  bool m_displayUpdateScheduled;


private:
  /** registers a input device */
  void setInputDevice(InputDevice * d);

public:
  explicit Application(int & argc, char ** argv);


  void addLayer(Layer * layer);

  /** sets the pre scaling of height map.
   *  The resulting heightmap will be rescaled as
   *  scaledMap = (inputMap + offset)*scaling
   */
  void setHeightScaling(float offset, float scaling);

public slots:
  void start();

private slots:

  void checkForRenderUpdates();
  void scheduleRenderPass();
  void renderLayers();

  void scheduleUpdates();
  void handleUpdates();

  void handleDepthInput();
  bool depth_change_necessary();

  void handleCamInput();

  void handleShutdown();

protected:



};

#endif // APPLICATION_H
