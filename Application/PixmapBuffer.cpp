#include <QtCore/QDebug>

#include "Application/PixmapBuffer.h"
#include "Layer/Layer.h"

PixmapBuffer::PixmapBuffer(Layer *l, Channels chan, const QSize &size)
  : QObject(l),
    m_channels(chan),
    m_width(0), m_height(0),
    m_opacity(1.0)
{

  this->initializeOpenGLFunctions();

  int type;
  switch(chan) {
    case RGB:
      type = CV_32FC3;
      break;
    case RGBA:
      type = CV_32FC4;
      break;

    default:
      Q_ASSERT(true);

    }

  if(size.isValid()) {
      m_buffer = cv::Mat(size.height(), size.width(), type);
      m_width = size.width();
      m_height = size.height();
    }

  glGenTextures(1, &m_tex);
  glBindTexture(GL_TEXTURE_2D, m_tex);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

}

void PixmapBuffer::setSize(QSize size)
{


  if(!size.isValid())
    return;

  qDebug() << "PixmapBuffer::setSize()   I:" << size;

  int type;
  switch(m_channels) {
    case RGB:
      type = CV_32FC3;
      break;
    case RGBA:
      type = CV_32FC4;
      break;

    default:
      Q_ASSERT(true);

    }


   m_buffer = cv::Mat(size.height(), size.width(), type);

   m_width = size.width();
   m_height = size.height();

}

cv::Mat &PixmapBuffer::buffer()
{
  Q_ASSERT_X(!m_buffer.empty(), "PixmapBuffer::buffer()",
             "Accessing a uninitialized buffer is not possible.");

  return m_buffer;
}

void PixmapBuffer::render()
{

  /* we only handle three or four channeled pixmaps */
  if(m_channels < RGB)
    return;

  glPushMatrix();

  glEnable(GL_TEXTURE_2D);

  glBindTexture(GL_TEXTURE_2D, m_tex);


  GLint pixelFormat = GL_RGB;
  if(m_channels == RGBA)
    pixelFormat = GL_RGBA;

  QPointF center = this->viewport().center();

  glTexImage2D(GL_TEXTURE_2D, 0, pixelFormat,
               m_width, m_height, 0,
               pixelFormat, GL_FLOAT, m_buffer.ptr());

  /* transform the center of the viewport */
  glTranslated(center.x(), center.y(), 0);

  glScalef(viewport().width()/2.0, viewport().height()/2.0, 1.0);

  glScalef(1.0, -1.0, 1);

  glEnable (GL_BLEND);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glBegin(GL_TRIANGLE_FAN);
  glColor4f(1.0, 1.0, 1.0, m_opacity);
  glTexCoord2f(0, 0); glVertex3f(-1,-1, 0);
  glTexCoord2f(1, 0); glVertex3f( 1,-1, 0);
  glTexCoord2f(1, 1); glVertex3f( 1, 1, 0);
  glTexCoord2f(0, 1); glVertex3f(-1, 1, 0);
  glEnd();

  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);

  glPopMatrix();



}
