#ifndef OPENGLWINDOW_H
#define OPENGLWINDOW_H

#include <QtCore/QTimer>
#include <QtGui/QWindow>
#include <QtGui/QOpenGLFunctions>
#include <QtGui/QOpenGLPaintDevice>


class Application;

class OpenGLWindow : public QWindow, protected QOpenGLFunctions
{
    Q_OBJECT

private:

  Application * m_app;

  bool m_update_pending;
  QOpenGLContext *m_context;
  bool m_needsInitialize;


protected:
  void resizeEvent(QResizeEvent * event);

public:
    explicit OpenGLWindow(Application * parent);
    ~OpenGLWindow();


    virtual void render();

    virtual void initialize();



public slots:
  /** calling this function will issue a re-rendering when the event loop is
   * idle
   */
  void renderLater();

  void renderNow();

protected:
    bool event(QEvent *event);

    void exposeEvent(QExposeEvent *event);

};
#endif // OPENGLWINDOW_H
