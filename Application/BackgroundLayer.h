#ifndef BACKGROUNDLAYER_H
#define BACKGROUNDLAYER_H

#include <QtCore/QMap>

#include "Layer/Layer.h"

class BackgroundLayer : public Layer
{
  Q_OBJECT

  typedef QMap<PropertyType, cv::Mat> BackgroundMap;
  QMap<PropertyType, cv::Mat> m_background;

public:
  explicit BackgroundLayer(Application *app);

  const cv::Mat materialProperty(PropertyType t);

  void update(PropertyTypes t, QRect rect) {}

signals:

public slots:

};

#endif // BACKGROUNDLAYER_H
