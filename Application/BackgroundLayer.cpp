#include <QtCore/QDebug>

#include "Application/BackgroundLayer.h"

BackgroundLayer::BackgroundLayer(Application * app) :
  Layer(app)
{
  /* fill background buffer for each property type available */
  for(int i = ePropertyTypeMin; i <= ePropertyTypeMax; i++) {
      m_background[(PropertyType)i] = this->createPropertyBuffer();
    }
}

const cv::Mat BackgroundLayer::materialProperty(Layer::PropertyType t)
{
  Q_ASSERT(m_background.contains(t));
  return const_cast<BackgroundMap&>(m_background)[t];
}
