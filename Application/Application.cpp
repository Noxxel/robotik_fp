#include <QtCore/QDebug>
#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QtCore/QTime>

#include "Application/Application.h"
#include "Application/BackgroundLayer.h"
#include "Input/TestInput.h"

/* Only include this header if we should build with kinect driver. */
#ifdef WITH_KINECTDEVICE
#include "Input/KinectDevice.h"
#endif


Application::Application(int & argc, char ** argv)
: m_app(argc, argv),
  m_window(0), m_input(0),
  m_inputOffset(0),
  m_inputScaling(1),
  m_propertyUpdateScheduled(false),
  m_displayUpdateScheduled(true)
{

  m_window = new OpenGLWindow(this);
  m_window->showMaximized();
  average = cv::Mat(480, 640,
                 CV_32F, 0.0);
  average.setTo(0);
  depthInputCounter = 0;

  /* set input device */
  /** @todo dont expose a interface to this but rather
   *        make it configurable via flags. */


  /* Check which input device we should use. */
#ifdef WITH_KINECTDEVICE
  KinectDevice * kinectDev = new KinectDevice(this);
  this->setInputDevice(kinectDev);
#else
  TestInput * testDev = new TestInput(this);
  this->setInputDevice(testDev);
#endif

  /* Creates a background layer to ensure that
   * there will be an endpoint for every material property. */
  BackgroundLayer * bg = new BackgroundLayer(this);
  this->addLayer(bg);

  /* Connect the display timer to periodicly check for updates. */
  connect(&m_displayTimer, &QTimer::timeout,
          this, &Application::checkForRenderUpdates);

  /* Lock frame rate to 25fps */
  m_displayTimer.setInterval(40);
  // m_displayTimer.setInterval(80); //can't display 25fps anyways

  /* Clean up stuff when the application is about to quit.
   * This can be triggered due to serveral events, such as system
   * signals or by closing the window etc... */
  connect(&m_app, &QGuiApplication::aboutToQuit,
          this, &Application::handleShutdown);

}

void Application::addLayer(Layer *layer)
{
  /* remember the previous layer */
  Layer * prev = 0;
  if(!m_layer.empty())
    prev = m_layer.last();

  m_layer.append(layer);
  layer->m_app = this;

  /* connect any signals for material property stuff here */
  if(layer != m_layer.first())
    {
      layer->m_prevLayer = prev;

      QObject::connect(layer, &Layer::materialPropertyChanged,
                       this, &Application::scheduleUpdates);

      QObject::connect(layer, &Layer::appearanceChanged,
                       this, &Application::scheduleRenderPass);
  }
}

void Application::setHeightScaling(float offset, float scaling)
{
  #ifdef WITH_KINECTDEVICE
    m_inputOffset = offset;
    m_inputScaling = scaling;
  #else
    m_inputScaling = 1;
    m_inputOffset = 0;
  #endif
}

void Application::setInputDevice(InputDevice *d)
{
  /* Connect signals and slots but make them queued, so that
   * we are guaranteed to process
   * the slot inside the simulation thread. */
  connect(d, &InputDevice::updatedDepth,
          this, &Application::handleDepthInput,
          Qt::QueuedConnection);

  connect(d, &InputDevice::updatedCamera,
          this, &Application::handleCamInput,
          Qt::QueuedConnection);


  m_input = d;
}

void Application::start()
{
  Q_ASSERT(m_input);

  m_input->start();
  m_displayTimer.start();
  m_window->showMaximized();
  m_app.exec();
}

void Application::checkForRenderUpdates()
{
  if(!m_displayUpdateScheduled)
    return;

  m_window->renderLater();
}

void Application::scheduleRenderPass()
{
  m_displayUpdateScheduled = true;
}

void Application::renderLayers()
{

  // qDebug() << "\n$>> Rendering <<$";

  // QTime timer;
  // timer.start();



  //adjust for misplaced beamer/kinect
  //glTranslatef(1,1,1);
  //glScalef(1,1,1);
  

  //update the vertexbuffer of all layers that had their resources updated
  if(!m_depthInput.empty()){
    glLoadIdentity();
    glScalef(1.22,1.235,1);
    glTranslatef(0.0,-0.23,0);
    for (int i = 0; i < m_layer.size(); ++i){
      if(m_layer[i]->m_visualChanged){
          Layer *l = m_layer[i];
          l->prepareRender(l->m_visualChangedRegion);
          l->m_visualChanged = false;
          l->m_visualChangedRegion = QRectF();
        }
    }
  }


  foreach(Layer * l, m_layer) {
      l->render();
  }
  /* unset flag again */
  m_displayUpdateScheduled = false;

  // qDebug() << "\n$<< (" << timer.elapsed() << " ms) >>$";
}

void Application::scheduleUpdates()
{

  /* Check if the flag is set. If so, an update is already
   * scheduled and we are done here. */
  if(m_propertyUpdateScheduled)
    return;

  /* Schedule a call to handlePropertyUpdate() as soon as the
   * event loop becomes idle again. */
  m_propertyUpdateScheduled = true;
  QTimer::singleShot(0, this, SLOT(handleUpdates()));
}

void Application::handleUpdates()
{

  // QTime timer;
  // timer.start();

  Layer::PropertyTypes changes(0);
  QRect changesRegion;

  /* Walk through all layers, update properties and collect changes. */
  foreach(Layer * l, m_layer) {

      /* remember any changes made so far */
      changes |= l->m_changed;
      changesRegion |= l->m_changedRegion;

      /* if there are no changes so far, we can skip the next calls */
      if(!changes)
        continue;

      /* call the layers update function */
      l->update(changes, changesRegion);

      /* There can be changes after a property update
       * which we need to collect here as well. */
      changes |= l->m_changed;
      changesRegion |= l->m_changedRegion;

      /* Unset changes in the currently processed layer again. */
      l->m_changed = Layer::PropertyTypes(0);
      l->m_changedRegion = QRect();
    }

  /* unset the flag again to ensure that a new update cycle can start */
  m_propertyUpdateScheduled = false;

  // qDebug() << "\n#<< (" << timer.elapsed() << " ms) >>#";
}

void Application::handleDepthInput()
{
  // average += ((m_input->depth()+m_inputOffset)*m_inputScaling);
  depthInputCounter++;

  if(depthInputCounter >= 5){
    depthInputCounter = 0;

    // average.copyTo(new_depth);
    // new_depth /= 5;
    // average.setTo(0);

    new_depth = ((m_input->depth()+m_inputOffset)*m_inputScaling);

    if(depth_change_necessary()){
      // qDebug() << "\n#>> DepthInput Iteration <<#";
      new_depth.copyTo(last_depth);

      cv::blur(new_depth, m_depthInput, cv::Size(10,10));

      //cut off the buggy edges for all layers
      for(int a = 0; a < 25; a++){
        m_depthInput.row(a).setTo(-4);
      }
      for(int a = 370; a < m_depthInput.rows; a++){
        m_depthInput.row(a).setTo(-4);
      }
      for(int a = 0; a < 145; a++){
        m_depthInput.col(a).setTo(-4);
      }
      for(int a = 485; a < m_depthInput.cols; a++){
        m_depthInput.col(a).setTo(-4);
      }

      double min, max;
      cv::Point minLoc, maxLoc;
      cv::minMaxLoc(m_depthInput, &min, &max, &minLoc, &maxLoc);

      // std::cout<<std::endl;
      // std::cout<<minLoc.y<<"  "<<minLoc.x<<std::endl;
      // std::cout<<min<<"  "<<max<<std::endl;

      if((max > 135 || min < -5) && max < 800){
        cv::blur(new_depth, m_depthInput, cv::Size(10,10));
        foreach(Layer * l, m_layer){
          l->updateDepthHigh(QRect());
        }
      }else if(!(max > 135 || min < -5)){
        m_depthInput.setTo(500, m_depthInput <= -4);
        foreach(Layer * l, m_layer){
          l->updateDepth(QRect());
        }
      }
    }
  }
}

bool Application::depth_change_necessary(){

  if(last_depth.empty()){
    return true;
  }

  float divergence = 0;
  float divergence_sum = 0;
  int counter = 0;

  for(int y=15; y<last_depth.rows-95; y++){
    for(int x=135; x<last_depth.cols-140; x++){
      divergence = new_depth.at<float>(y,x)-last_depth.at<float>(y,x);
      if(std::abs(divergence) > 10){
        divergence_sum++;
      }
      counter++;
    }
  }

  // float divergence = divergence_sum/((last_depth.cols-235)*last_depth.rows);

  return (divergence_sum/counter > 0.0025);

}

void Application::handleCamInput()
{

}

void Application::handleShutdown()
{
    QList<QObject*> children = this->children();
    foreach(QObject * c, children) {
        delete c;
    }
}