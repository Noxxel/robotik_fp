#ifndef PIXMAPBUFFER_H
#define PIXMAPBUFFER_H

#include <QtCore/QRectF>
#include <QtGui/QOpenGLFunctions>

#include <opencv2/core/core.hpp>


class Layer;

/**
 * A pixmap buffer class will be used to render openCV matrices
 * into an openGL context with the help of vertex buffer objects.
 * This class will hide away all hardware handling.
 *
 * Before you can use this buffer, you have to call setSize()
 * on the buffer instance.
 *
 * You can specify a viewport. By default it is set to the
 * full viewport used by scene manager. See viewport() for further
 * reference.
 *
 * This buffer will be mapped into CPU memory where it can be accessed by
 * buffer() and then be modified with openCVs matrix operations.
 *
 * @note The mapping is only valid during a Layer::prepareRender() call.
 *       Reading or writing from anywhere else
 *       will lead to unexpected behaviour.
 *
 */
class PixmapBuffer : public QObject, protected QOpenGLFunctions
{

  Q_OBJECT

  friend class Layer;
  friend class Application;

  /* texture buffer id */
  GLuint m_tex;

  /* target viewport */
  QRectF m_viewport;

public:
  /**
   * @brief number of channels this pixmap buffer holds for each pixel
   */
  enum Channels {
    RGB = 3,
    RGBA = 4
  };

private:

  cv::Mat m_buffer;
  Channels m_channels;
  float m_opacity;

  int m_width;
  int m_height;

public:


  /**
   * @param type openCV matrix type that should be requested
   */
  PixmapBuffer(Layer * l, Channels chan = RGB, const QSize & size = QSize());


  /** Sets the size of the pixmap.
   *  Note that you cannot change the number of channels here */
  void setSize(QSize size);

  /** @overload */
  void setSize(int width, int height);

  /** @brief renders the requested matrix into openGL context */
  void render();

  QRectF viewport() const {
    return m_viewport;
  }
  void setViewport(const QRectF & viewport)  {
    m_viewport = viewport;
  }

  float opacity() const {
    return m_opacity;
  }

  void setOpacity(float opacity) {
    Q_ASSERT(opacity >= 0.0 && opacity <= 1.0);
    m_opacity = opacity;
  }




  /**
   * @brief buffer
   * @return a buffer pointing to the mapped memory region.
   * @warning Returned buffer is only valid
   *          inside Layer::prepareRender() call.
   */
  cv::Mat & buffer();


};

#endif // PIXMAPBUFFER_H
