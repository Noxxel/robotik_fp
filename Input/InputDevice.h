#ifndef INPUTDEVICE_H
#define INPUTDEVICE_H

#include <QtCore/QObject>
#include <opencv2/core/core.hpp>

class Application;

class InputDevice : public QObject
{
  Q_OBJECT

public:

  virtual QSize depthSize() const = 0;
  virtual QSize camSize() const = 0;

  virtual const cv::Mat depth() = 0;
  virtual const cv::Mat cam() = 0;
signals:

  void updatedDepth();
  void updatedCamera();

  void started();

public slots:

  virtual void start() = 0;


};

#endif // INPUTDEVICE_H
