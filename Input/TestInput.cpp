#include <QtCore/QDebug>
#include <iostream>

#include "Input/TestInput.h"
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;

TestInput::TestInput(Application *app)
  : m_progress(0.2)
{
  static int sizeX = 640;
  static int sizeY = 480;
  m_depth = cv::Mat(sizeY, sizeX, CV_32F);
  m_cam = cv::Mat(sizeY, sizeX, CV_8UC3);

  m_depth = 0.0;
  m_cam = Scalar(0, 0, 0);
}

QSize TestInput::depthSize() const
{
//  qDebug() << "TestInput::depthSize():"
//           << QSize(m_depth.size().width,
//                   m_depth.size().height);
  return QSize(m_depth.size().width,
               m_depth.size().height);
}

QSize TestInput::camSize() const
{
  return QSize(m_cam.size().width,
               m_cam.size().height);
}

const cv::Mat TestInput::depth()
{
  return m_depth;
}

const cv::Mat TestInput::cam()
{
  return m_cam;
}

void TestInput::start()
{
  qDebug() << "TestInput::starting(): ";
  this->startTimer(250);
  return;
}

void TestInput::timerEvent(QTimerEvent *event)
{

  // m_progress += 0.01;
  // if(m_progress >= 1.0)
  //   m_progress = 0.2;


  double sigmaX = m_progress*0.75*m_depth.size().width;
  double sigmaY = m_progress*0.55*m_depth.size().height;

  /* create gaussian peak to simulate a mountain */
  cv::Mat kernelX = cv::getGaussianKernel(m_depth.size().width,
                                          sigmaX,
                                          CV_32F);
  cv::Mat kernelY = cv::getGaussianKernel(m_depth.size().height,
                                          sigmaY,
                                          CV_32F);
  m_depth = kernelY * kernelX.t();


  /* find maximum */
  double min, max;
  cv::minMaxLoc(m_depth, &min, &max);

  /* normalize the values such that the maximum as height of 1.0 */

  if(max > 0.0)
    m_depth = ((95*m_depth)/max);

  emit updatedDepth();
}

