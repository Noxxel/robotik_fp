#ifndef TESTINPUT_H
#define TESTINPUT_H

#include <QtCore/QSize>

#include <opencv2/core/core.hpp>

#include "Input/InputDevice.h"

class TestInput : public InputDevice
{
  Q_OBJECT


  cv::Mat m_depth;
  cv::Mat m_cam;

  double m_progress;

public:
  explicit TestInput(Application *app );

  QSize depthSize() const;
  QSize camSize() const;

  const cv::Mat depth();
  const cv::Mat cam();

signals:

public slots:

  void start();

protected:
  void timerEvent(QTimerEvent * event);

};

#endif // TESTINPUT_H
