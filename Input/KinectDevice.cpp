
#include <QtCore/QDebug>
#include <QtCore/QMutexLocker>

#include "Input/KinectDevice.h"
#include "Application/Application.h"

KinectDevice::KinectDevice(Application *app) :
    InputDevice(), m_worker(0)
{

    /* prepare input buffer */
    int sizex = 640;
    int sizey = 480;
    m_depth = cv::Mat(sizey, sizex, CV_32F);
    m_depth = 0.0;
    m_cam = cv::Mat(sizey, sizex, CV_8UC3);
    m_cam = cv::Scalar(0,0,0);


    m_worker = new Thread(this);

    int ret = freenect_init(&f_ctx, NULL);
    Q_ASSERT(ret >= 0);

    // Show debug messages and use camera only.
    freenect_set_log_level(f_ctx, FREENECT_LOG_DEBUG);
    freenect_select_subdevices(f_ctx, FREENECT_DEVICE_CAMERA);

    // Find out how many devices are connected.
    int num_devices = ret = freenect_num_devices(f_ctx);
    Q_ASSERT(ret >= 0);


    if (num_devices == 0)
    {
        printf("No device found!\n");
        freenect_shutdown(f_ctx);
        Q_ASSERT(false);
    }

    // Open the first device.
    ret = freenect_open_device(f_ctx, &f_dev, 0);
    if (ret < 0) {
        freenect_shutdown(f_ctx);
        Q_ASSERT(false);
    }


    // Set depth and video modes.
    ret = freenect_set_depth_mode(
                f_dev,
                freenect_find_depth_mode(FREENECT_RESOLUTION_MEDIUM,
                                         FREENECT_DEPTH_11BIT)
                );

    if (ret < 0) {
        freenect_shutdown(f_ctx);
        Q_ASSERT(false);
    }

    ret = freenect_set_video_mode(
                f_dev,
                freenect_find_video_mode(FREENECT_RESOLUTION_MEDIUM,
                                         FREENECT_VIDEO_RGB)
          );

    if (ret < 0) {
        freenect_shutdown(f_ctx);
        Q_ASSERT(false);
    }


    // Set frame callbacks.
    freenect_set_depth_callback(f_dev, KinectDevice::depth_cb);
    freenect_set_video_callback(f_dev, KinectDevice::video_cb);

    /* set the this pointer to the device struct to know the current
     * instance in callbacks */
    freenect_set_user(f_dev, this);



}

KinectDevice::~KinectDevice()
{
    delete m_worker;
}

void KinectDevice::depth_cb(freenect_device *dev,
                            void *data,
                            unsigned int timestamp)
{

    KinectDevice * cur = (KinectDevice*)freenect_get_user(dev);

    if(!cur)
        return;

    QMutexLocker lock(&cur->m_mutex);

    uint16_t *depth = (uint16_t*)data;

    int sizex = 640;
    int sizey = 480;

    /* convert integer input into normalized floats
     * with respect to the input scale */
    for(int i = 0; i < sizex*sizey; i++) {
        cur->m_depth.ptr<float>()[i] = 1.0-depth[i]/2048.0;
    }

    /* note that is is not the usual emit syntax... */
    emit cur->updatedDepth();

//    qDebug() << "Depth Callback:" << timestamp << cur << min << max;
}

void KinectDevice::video_cb(freenect_device *dev,
                            void *data,
                            unsigned int timestamp)
{
    /** @todo implement me. have a look into KinectDevice::depth_cb() */
}

QSize KinectDevice::depthSize() const
{
    QMutexLocker lock(const_cast<QMutex*>(&m_mutex));
    return QSize(m_depth.size().width,
                 m_depth.size().height);
}

QSize KinectDevice::camSize() const
{
    QMutexLocker lock(const_cast<QMutex*>(&m_mutex));
    return QSize(m_cam.size().width,
                 m_cam.size().height);
}

const cv::Mat KinectDevice::depth()
{
    QMutexLocker lock(&m_mutex);
    return m_depth;
}

const cv::Mat KinectDevice::cam()
{
    QMutexLocker lock(&m_mutex);
    return m_cam;
}

void KinectDevice::start()
{
    m_worker->start();
}

