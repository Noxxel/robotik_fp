#ifndef KINECTDEVICE_H
#define KINECTDEVICE_H

#include <QtCore/QObject>
#include <QtCore/QThread>
#include <QtCore/QMutex>

#include <libfreenect.h>


#include "Input/InputDevice.h"

class Application;

class KinectDevice : public InputDevice
{
    friend class Thread;

    class Thread : public QThread {

        bool quit;
        KinectDevice * d;

    public:
        Thread(KinectDevice * d)
            : quit(false), d(d)
        {
        }

        ~Thread() {
            quit = true;
            this->wait();
        }

        void run() {

            freenect_start_depth(d->f_dev);
//            freenect_start_video(d->f_dev);

            timeval timeout;
            timeout.tv_sec = 0;
            timeout.tv_usec = 1000;

            while(!quit) {
                if(freenect_process_events_timeout(d->f_ctx, &timeout) < 0)
                    break;

            }

            freenect_stop_depth(d->f_dev);
//            freenect_stop_video(d->f_dev);
            freenect_close_device(d->f_dev);
            freenect_shutdown(d->f_ctx);
        }
    };



    Q_OBJECT

    freenect_context *f_ctx;
    freenect_device *f_dev;
    freenect_video_format requested_format = FREENECT_VIDEO_RGB;
    freenect_video_format current_format = FREENECT_VIDEO_RGB;

    Thread * m_worker;
    QMutex m_mutex;


    cv::Mat m_depth;
    cv::Mat m_cam;



private:
    static void depth_cb(freenect_device* dev,
                  void* data,
                  unsigned int timestamp);

    static void video_cb(freenect_device* dev,
                  void* data,
                  unsigned int timestamp);

public:
    explicit KinectDevice(Application *app);
    ~KinectDevice();


    QSize depthSize() const;
    QSize camSize() const;

    const cv::Mat depth();
    const cv::Mat cam();


signals:

public slots:
    void start();

};

#endif // KINECTDEVICE_H
